package com.example.sharemeals;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.sharemeals.adapters.MealAdapter;
import com.example.sharemeals.models.Meal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FragmentTabUserPickup extends Fragment {

    RecyclerView.LayoutManager layoutManager;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals_wait_for_picking");
    DatabaseReference meals_His = pickupMeals.getReference("meals_His");
    List<Meal> mealsList = new ArrayList<>();
    RecyclerView recyclerView;
    ProgressBar progressBar ;
    MealAdapter mealAdapter;
    Bitmap bitmap;
    View rootview;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_tab_user_pickup,container,false);
        user  = firebaseAuth.getCurrentUser();
        recyclerView = rootview.findViewById(R.id.user_pickup_recyclerview);
//        progressBar = rootview.findViewById(R.id.progress_circular);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        mealAdapter = new MealAdapter(mealsList,getContext());
        AlertDialog.Builder builder_service  = new AlertDialog.Builder(recyclerView.getContext());
        builder_service.setTitle("start chat with user").setMessage("האם לעשות את העדכון בצורה אוטומטית?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        }).show();
        mealAdapter.setListener(new MealAdapter.myMealListener() {
            @Override
            public void onMealClicked(final int position, View view) {
                AlertDialog.Builder builder  = new AlertDialog.Builder(recyclerView.getContext());
                builder.setTitle("start chat with user").setMessage("האם תרצה להתחיל צ'ט עם מעלה המנה?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }).show();

            }
        });
        meals.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mealsList.clear();
                if (dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        final Meal mealFromdb = snapshot.getValue(Meal.class);
                        new Thread(new Runnable() {
                            public void run() {
                                if (mealFromdb.getPickup_by().equals(user.getUid())){
                                    mealsList.add(mealFromdb);
                                }

                            }
                        }).start();
                    }
                    mealAdapter = new MealAdapter(mealsList,getContext());
                    recyclerView.setAdapter(mealAdapter);
                    mealAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        final ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.DOWN|ItemTouchHelper.UP, ItemTouchHelper.LEFT ) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {
                int drag_pos = dragged.getAdapterPosition();
                int target_pos = target.getAdapterPosition();
                Collections.swap(mealsList , drag_pos , target_pos);
                mealAdapter.notifyItemMoved(drag_pos,target_pos);
                return false;

            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
                AlertDialog.Builder builder  = new AlertDialog.Builder(recyclerView.getContext());
                builder.setTitle("confirm remvove").setMessage("are you sure that you to remove item?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                meals_His.child(mealsList.get(viewHolder.getAdapterPosition()).getMeal_id()).setValue(mealsList.get(viewHolder.getAdapterPosition()));
                                meals.child(mealsList.get(viewHolder.getAdapterPosition()).getMeal_id()).removeValue();
                                mealsList.remove(viewHolder.getAdapterPosition());
                                mealAdapter.notifyDataSetChanged();

                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mealAdapter.notifyDataSetChanged();
                    }
                }).show();



            }
        };
        return rootview;
    }
}
