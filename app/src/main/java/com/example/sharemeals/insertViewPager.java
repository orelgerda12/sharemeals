package com.example.sharemeals;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.sharemeals.adapters.insertAdapter;

import org.w3c.dom.Text;

public class insertViewPager extends Fragment {
    View rootView;
    Button go;
    ViewPager viewPager;
    insertAdapter adapter;
    interface OnWelcomeFragmentListener{
        void onGoToLogin();
    }
    OnWelcomeFragmentListener callback;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (OnWelcomeFragmentListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException("the activity isnt implemen OnSignUpFragmentListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.insert_view_pagner,container,false);
        go = rootView.findViewById(R.id.btn_go);
        viewPager = rootView.findViewById(R.id.insert_view_pager);
        adapter = new insertAdapter(rootView.getContext());
        viewPager.setAdapter(adapter);
         go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onGoToLogin();
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i==1){
                    go.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        return  rootView;
    }
}
