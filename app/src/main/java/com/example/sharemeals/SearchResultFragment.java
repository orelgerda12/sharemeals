package com.example.sharemeals;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.sharemeals.adapters.SearchResultPageAdapter;
import com.example.sharemeals.models.Meal;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.List;

public class SearchResultFragment extends Fragment {
    List<Meal> ResultMealList;
    SearchResultPageAdapter searchResultPageAdapter;
    RecyclerView ResultrV;
    ViewPager viewPager;
    View rootview;
    ImageButton pick_meal;
    String choose , pick_by_userid;
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals_wait_for_picking = pickupMeals.getReference("meals_wait_for_picking");
    DatabaseReference meals = pickupMeals.getReference("meals");
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    Meal ChosenMeal;
    int currentPosition;


    public static SearchResultFragment newinstance(List<Meal> mealList){
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();
       args.putSerializable("meal_list", (Serializable) mealList);
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.search_result_view,container,false);
        ResultMealList = (List<Meal>)getArguments().getSerializable("meal_list");
        user = firebaseAuth.getCurrentUser();
        pick_by_userid = user.getUid();
        viewPager = rootview.findViewById(R.id.view_pager);
        pick_meal = rootview.findViewById(R.id.choose_meal);
        viewPager.setPageTransformer(true , new DepthPageTransformer());
//        ResultrV.setLayoutManager(new LinearLayoutManager(getActivity()));
//        ResultrV.setHasFixedSize(true);
        searchResultPageAdapter = new SearchResultPageAdapter(ResultMealList,rootview.getContext());
        viewPager.setAdapter(searchResultPageAdapter);
        searchResultPageAdapter.notifyDataSetChanged();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                currentPosition = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        pick_meal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChosenMeal = ResultMealList.get(currentPosition);
                ChosenMeal.setPickup_by(pick_by_userid);
                meals_wait_for_picking.child(ChosenMeal.getMeal_id()).setValue(ChosenMeal).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.take_doante_success),Snackbar.LENGTH_LONG).show();
                        meals.child(ChosenMeal.getMeal_id()).removeValue();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.take_doante_fail),Snackbar.LENGTH_LONG).show();
                    }
                });

            }
        });


//        searchResultPageAdapter = new MealAdapter(ResultMealList, rootview.getContext());
//        ResultrV.setAdapter(searchResultPageAdapter);
//        searchResultPageAdapter.setListener(new MealAdapter.myMealListener() {
//            @Override
//            public void onMealClicked(int position, View view) {
//                choos = ResultMealList.get(position).getName();
//                Toast.makeText(getContext(), choos, Toast.LENGTH_SHORT).show();
//
//            }
//        });
    return rootview;
    }
}
