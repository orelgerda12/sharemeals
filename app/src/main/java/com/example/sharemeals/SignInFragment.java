package com.example.sharemeals;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class SignInFragment extends Fragment {

    TextInputLayout login_username , login_password;
    Button login_btn , register_btn;


    interface OnSignInFragmentListener{
        void onSignIn(String username , String Password);
        void onRegBtn();
    }
    OnSignInFragmentListener callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (OnSignInFragmentListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException("the activity isnt implemen OnSignUpFragmentListener");
        }
    }
    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview;
        rootview = inflater.inflate(R.layout.sign_in_layout,container,false);
        login_btn = rootview.findViewById(R.id.sign_in);
        register_btn = rootview.findViewById(R.id.sign_up_btn);
        login_username = rootview.findViewById(R.id.username_login);
        login_password = rootview.findViewById(R.id.password_login);
        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onRegBtn();
            }
        });
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username;
                String Password;
                username = login_username.getEditText().getText().toString().trim();
                Password = login_password.getEditText().getText().toString().trim();
                callback.onSignIn(username,Password);
            }
        });

        return rootview;
    }
}
