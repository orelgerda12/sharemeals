package com.example.sharemeals.search_fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sharemeals.R;
import com.example.sharemeals.adapters.MealAdapter;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private List<Category> categoryModel;
    public CategoryAdapter(List<Category> categoryModel){
        this.categoryModel = categoryModel;
    }
    myCategoryListener listener;
    interface myCategoryListener{
        void onCategoryClicked(int position , View view);
    }
    public void setListener(myCategoryListener listener){
        this.listener = listener;
    }
    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        TextView name ;
        ImageView image;

        public CategoryViewHolder(@NonNull final View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_category_text_view);
            image = itemView.findViewById(R.id.item_category_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null)
                        listener.onCategoryClicked(getAdapterPosition(),v);
                }
            });
        }

    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_category_items,viewGroup,false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        Category model = categoryModel.get(i);
        categoryViewHolder.image.setImageResource(model.getImage_link());
        categoryViewHolder.name.setText(model.getName());
    }

    @Override
    public int getItemCount() {
        return categoryModel.size();
    }
}
