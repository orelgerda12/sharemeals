package com.example.sharemeals.search_fragment;

public class Category {
    private String Name;
    private int image_link;

    public Category(String name, int image_link) {
        Name = name;
        this.image_link = image_link;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getImage_link() {
        return image_link;
    }

    public void setImage_link(int image_link) {
        this.image_link = image_link;
    }
}
