package com.example.sharemeals.search_fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharemeals.R;
import com.example.sharemeals.adapters.MealAdapter;
import com.example.sharemeals.models.Meal;
import com.example.sharemeals.models.User;
import com.example.sharemeals.signUpFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class SearchMealsFragment extends Fragment {
    View rootview;
    AutoCompleteTextView cityAuto , areaAuto;
    CheckBox kosherCb;
    String categoryClicked;
    RecyclerView categoryRecylerview;
    RecyclerView MealsRecyclerview;
    ProgressBar progressBar;
    ImageButton imageButton;
    String[] name = {"Americain", "Dessert" , "Fast_food" , "Isreali" , "Thai" ,"Vegan", "Mexican" , "Soups", "Italian"};
    int[] resource = {R.drawable.american,R.drawable.dessert,
            R.drawable.fast_food ,R.drawable.israeli , R.drawable.thai , R.drawable.vegan
    ,R.drawable.mexican ,R.drawable.soups, R.drawable.italain};
    List<Category> category = new ArrayList<>();
    List<Meal> mealsList = new ArrayList<>();
    CategoryAdapter categoryAdapter;
    MealAdapter mealAdapter;
    FirebaseUser user;
    TextView text_cat;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference mealsRef = pickupMeals.getReference("new_meals");
    public interface OnMealsResultListener{
        void OnMealsResult(List<Meal> mealList);
    }
    OnMealsResultListener ResultCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            ResultCallback = (OnMealsResultListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException("the activity isnt implemen OnMealsResultListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.search_meals_fragment, container , false);
        user = firebaseAuth.getCurrentUser();
        categoryRecylerview = rootview.findViewById(R.id.search_category_recyclerview);
//        progressBar = rootview.findViewById(R.id.search_progress_circle);
        areaAuto = rootview.findViewById(R.id.search_area_autocomplete);
        cityAuto = rootview.findViewById(R.id.search_city_autocomplete);
        String[] CityList = getResources().getStringArray(R.array.cities_heb);
        String[] AreaList = getResources().getStringArray(R.array.area_listheb);
        text_cat = rootview.findViewById(R.id.text_category);
        ArrayAdapter<String> AreaautoCompleteaAdapter;
        AreaautoCompleteaAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, AreaList);
        areaAuto.setAdapter(AreaautoCompleteaAdapter);
        ArrayAdapter<String> CityautoCompleteaAdapter;
        CityautoCompleteaAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, CityList);
        cityAuto.setAdapter(CityautoCompleteaAdapter);
        kosherCb = rootview.findViewById(R.id.search_kosher_cb);
        imageButton = rootview.findViewById(R.id.search_donation_btn);
        LinearLayoutManager linearLayout = new LinearLayoutManager(getActivity(), LinearLayout.HORIZONTAL,false);
        categoryRecylerview.setLayoutManager(linearLayout);
        categoryRecylerview.setHasFixedSize(true);
//        MealsRecyclerview = rootview.findViewById(R.id.search_result_recycleview);
//        MealsRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
//        MealsRecyclerview.setHasFixedSize(true);
        for(int i =0 ; i< name.length;i++){
            Category categoryObj = new Category(name[i],resource[i]);
            category.add(i,categoryObj);
        }
        categoryAdapter = new CategoryAdapter(category);
        mealAdapter = new MealAdapter(mealsList,getContext());
        categoryRecylerview.setAdapter(categoryAdapter);
        categoryAdapter.setListener(new CategoryAdapter.myCategoryListener() {
            @Override
            public void onCategoryClicked(int position, View view) {
                categoryClicked = category.get(position).getName();
                text_cat.setText(categoryClicked);
            }


        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mealsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mealsList.clear();
                        if (dataSnapshot.exists()){
                            for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                                Boolean flag = true;
                                Meal mealFromdb = snapshot.getValue(Meal.class);
                                for (String s:  WhoIsNotNull()){
                                    switch (s){
                                        case ("city"):
                                            if(!(cityAuto.getText().toString().equals(mealFromdb.getCity())))
                                                flag = false;
                                            break;
                                        case ("area"):
                                            if(!(areaAuto.getText().toString().equals(mealFromdb.getArea())))
                                                flag = false;
                                            break;
                                        case("category"):
                                            if (!(categoryClicked.equals(mealFromdb.getCategory())))
                                                flag = false;
                                            break;
                                        case("kosher"):
                                            if (kosherCb.isChecked()!=mealFromdb.getKosher())
                                                flag = false;
                                            break;
                                    }
                                }
                                if(flag)
                                    mealsList.add(mealFromdb);
                            }
                            ResultCallback.OnMealsResult(mealsList);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });


            }
        });

        return rootview;
    }

    public class SearchAsyncTask extends AsyncTask<Void , Void , List<Meal>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Meal> doInBackground(Void... voids) {
            mealsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mealsList.clear();
                    if (dataSnapshot.exists()){
                        for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Boolean flag = true;
                            Meal mealFromdb = snapshot.getValue(Meal.class);
                            for (String s:  WhoIsNotNull()){
                                switch (s){
                                    case ("city"):
                                        if(!(cityAuto.getText().toString().equals(mealFromdb.getCity())))
                                            flag = false;
                                        break;
                                    case ("area"):
                                        if(!(areaAuto.getText().toString().equals(mealFromdb.getArea())))
                                            flag = false;
                                        break;
                                    case("category"):
                                        if (!(categoryClicked.equals(mealFromdb.getCategory())))
                                            flag = false;
                                        break;
                                    case("kosher"):
                                        if (kosherCb.isChecked()!=mealFromdb.getKosher())
                                            flag = false;
                                        break;
                                }
                            }
                            if(flag)
                                mealsList.add(mealFromdb);
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            return mealsList;

        }
        @Override
        protected void onPostExecute(List<Meal> mealsList) {
            super.onPostExecute(mealsList);
            ResultCallback.OnMealsResult(mealsList);
            progressBar.setVisibility(View.GONE);

        }
    }
     public ArrayList<String> WhoIsNotNull(){
        ArrayList<String> NotNullArtributs = new ArrayList<>();
        String area = areaAuto.getText().toString();
        String city = cityAuto.getText().toString();
        String category = categoryClicked;
        Boolean kosher = kosherCb.isChecked();
         if(!(area.isEmpty())){
             NotNullArtributs.add("area");
        }
        if (!(city.isEmpty())){
            NotNullArtributs.add("city");
        }
        if(categoryClicked!=null)
            NotNullArtributs.add("category");
        if(kosher)
            NotNullArtributs.add("kosher");
        return NotNullArtributs;
     }


}
