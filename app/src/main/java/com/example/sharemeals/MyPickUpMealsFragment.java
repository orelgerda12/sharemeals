package com.example.sharemeals;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.example.sharemeals.adapters.MealAdapter;
import com.example.sharemeals.models.Meal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MyPickUpMealsFragment extends Fragment {


    RecyclerView.LayoutManager layoutManager;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals");
    List<Meal> mealsList = new ArrayList<>();
    MealAdapter adapter;
    RecyclerView recyclerView;
    ProgressBar progressBar ;




    public static MyPickUpMealsFragment newInstance() {
        MyPickUpMealsFragment fragment = new MyPickUpMealsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview;
        user  = firebaseAuth.getCurrentUser();
        rootview = inflater.inflate(R.layout.my_pickup_meals_layout,container,false);
        recyclerView = rootview.findViewById(R.id.my_pickup_meals_recycle_view);
        progressBar = rootview.findViewById(R.id.progress_circular);
        LinearLayoutManager linearLayout = new LinearLayoutManager(getActivity(),LinearLayout.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setHasFixedSize(true);
        loadData();
        return rootview;
    }
    public void loadData(){
        progressBar.setVisibility(View.VISIBLE);
        meals.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mealsList.clear();
                if (dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Meal meal = snapshot.getValue(Meal.class);
                            mealsList.add(meal);
                    }
                    adapter = new MealAdapter(mealsList, getContext());
                    recyclerView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

