package com.example.sharemeals;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.sharemeals.adapters.ConversationAdapter;
import com.example.sharemeals.adapters.MealAdapter;
import com.example.sharemeals.chat.ChatManager;
import com.example.sharemeals.models.ChatMessage;
import com.example.sharemeals.models.ChatMessageWrapper;
import com.example.sharemeals.models.Conversation;
import com.example.sharemeals.models.Meal;
import com.example.sharemeals.view_model.ChatViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;



import static com.example.sharemeals.MainActivity.messagesList;
import static com.google.gson.reflect.TypeToken.get;

public class ConversationFragment extends Fragment {


    RecyclerView.LayoutManager layoutManager;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals");
    LinkedList<ChatMessage> mMessagesList = new LinkedList<>();
    ConversationAdapter adapter;
    RecyclerView recyclerView;
    ProgressBar progressBar ;
    BroadcastReceiver receiver;
    private EditText mTextToSendView;
    private DBListener mMessageDBListener;
    private static final String SECOND_USER_TOKEN = "secondUserToken";
    private static final String SECOND_USER_ID = "secondUserId";
    private static final String SECOND_USER_NAME = "secondUserName";
    final GenericTypeIndicator<ArrayList<Conversation>> mType = new GenericTypeIndicator<ArrayList<Conversation>>(){};
    private  DatabaseReference mConversation;
    //private int mThisConversationIndex;
    private String mSecondUserToken;
    private String mSecondUserName;
    private String mSecondUserID;
    public ChatViewModel mChatViewModel;
    private boolean isMessageSent;

    public interface OnSendNewMessageListener{
        void onSendNewMessage(String message, String destToken);
    }

    public static ConversationFragment newInstance(String secondUserId, String secondUserToken,String secondUserName) {
        ConversationFragment fragment = new ConversationFragment();
        Bundle args = new Bundle();
        args.putString(ConversationFragment.SECOND_USER_ID,secondUserId);
        args.putString(ConversationFragment.SECOND_USER_TOKEN,secondUserToken);
        args.putString(ConversationFragment.SECOND_USER_NAME,secondUserName);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview;
        final ChatViewModel chatViewModel = ViewModelProviders.of(getActivity()).get(ChatViewModel.class);
        //mThisConversationIndex = getArguments().getString(ConversationFragment.SECOND_USER_ID);
        mSecondUserID = getArguments().getString(ConversationFragment.SECOND_USER_ID);
        mSecondUserToken = getArguments().getString(ConversationFragment.SECOND_USER_TOKEN);
        mSecondUserName = getArguments().getString(ConversationFragment.SECOND_USER_NAME);
        initializeViewModel();
        ChatViewModel mChatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);


        //mThisConversationIdLength = mThisConversationId.length()+"#";
        //mConversation = FirebaseDatabase.getInstance().getReference("conversations").child(mThisConversationId);
        //user  = firebaseAuth.getCurrentUser();

        rootview = inflater.inflate(R.layout.conversation_fragment_layout,container,false);
        recyclerView = rootview.findViewById(R.id.my_pickup_meals_recycle_view);
        progressBar = rootview.findViewById(R.id.progress_circular);
        mTextToSendView = rootview.findViewById(R.id.editText_message_to_send);

        //recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerView.setHasFixedSize(true);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(manager);
        rootview.findViewById(R.id.btn_send_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //messagesList.add(new ChatMessage(mTextToSendView.getText().toString(),true));

                //mConversation.setValue(new ChatMessage(mTextToSendView.getText().toString(),"","",mThisUSerId));
                //((MainActivity)getActivity()).send(mThisUSerId+"!"+mThisConversationIdLength+mThisConversationId+":"+mTextToSendView.getText().toString());
                //mMessagesList.offerFirst(new ChatMessage(mTextToSendView.getText().toString(),"","","",true));
                //adapter.notifyDataSetChanged();
                chatViewModel.selectSendNewMessage(new ChatMessageWrapper(new ChatMessage(mTextToSendView.getText().toString(),"","",ChatManager.thisUserId,true),
                                                    mSecondUserID,mSecondUserToken));
                isMessageSent = true;
            }
        });
//        messagesList.add(new ChatMessage("Message",true));
//        messagesList.add(new ChatMessage("Message",false));
//        messagesList.add(new ChatMessage("Message",true));
//        messagesList.add(new ChatMessage("Message",false));
//        messagesList.add(new ChatMessage("",true));
//        messagesList.add(new ChatMessage("Message",false));
//        messagesList.add(new ChatMessage("",true));
//        messagesList.add(new ChatMessage("Message",false));
//        messagesList.add(new ChatMessage("Message",true));
//        messagesList.add(new ChatMessage("Message",false));
//        messagesList.add(new ChatMessage("Message",true));
//        messagesList.add(new ChatMessage("Message",false));
//        messagesList.add(new ChatMessage("Message",true));

        adapter = new ConversationAdapter(ChatManager.messagesMap.get(mSecondUserID));
        recyclerView.setAdapter(adapter);

        //getPreviousMessages();


        ((MainActivity)getActivity()).setAdapter(adapter);  ///must fix
        return rootview;
    }

//    private void setBroadCastReceiver(){
//        receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                //messageTv.setText(intent.getStringExtra("message"));
//                messagesList.add(new ChatMessage(intent.getStringExtra("message"),false));
//                adapter.notifyDataSetChanged();
//            }
//        };
//
//        IntentFilter filter = new IntentFilter("message_received");
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,filter);
//    }

    private void getPreviousMessages(){
        mConversation.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                ArrayList<ChatMessage> mes;
                if(dataSnapshot.exists())
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        mMessagesList.add(snapshot.getValue(ChatMessage.class));
                    }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        /*if(isMessageSent){
            ChatMessage mes = mMessagesList.remove(mThisConversationIndex);
            mMessagesList.offerFirst(mes);
        }*/

        //FirebaseDatabase.getInstance().getReference("users").child(ChatManager.thisUserId).child("conversationData").child(mSecondUserID).setValue(mMessagesList);
        //LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        ((MainActivity)getActivity()).removeAdapter();

    }

    private void initializeViewModel(){
        mChatViewModel = ViewModelProviders.of(getActivity()).get(ChatViewModel.class);
        mChatViewModel.getUpdateAdapter().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String messages) {
                   adapter.notifyDataSetChanged();
            }
       });
       // mChatViewModel.getMessages(ChatManager.thisUserId,mSecondUserID).observe(this, new Observer<LinkedList<ChatMessage>>() {
//            @Override
//            public void onChanged(@Nullable LinkedList<ChatMessage> messages) {
//               if(messages != null) {
//                   mMessagesList = new LinkedList<>(messages);
//                   adapter.notifyDataSetChanged();
//               }
//            }
//        });


    }
    public void loadData(){
        /*progressBar.setVisibility(View.VISIBLE);
        meals.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mealsList.clear();
                if (dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Meal meal = snapshot.getValue(Meal.class);
                        //mealsList.add(meal);
                    }
                    adapter = new ConversationAdapter(0);
                    recyclerView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
    }
}

