package com.example.sharemeals;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sharemeals.adapters.MealAdapter;
import com.example.sharemeals.models.Meal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.ComparisonChain.start;

public class FragmentTabUserDonation extends Fragment {
    View rootview;
    RecyclerView.LayoutManager layoutManager;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("new_meals");
    DatabaseReference meals_wait = pickupMeals.getReference("meals_wait_for_picking");
    DatabaseReference meals_His = pickupMeals.getReference("meals_His");
    List<Meal> mealsList = new ArrayList<>();
//    MealAdapter adapter;
    RecyclerView recyclerView;
    ProgressBar progressBar ;
    MealAdapter mealAdapter;
    Bitmap bitmap;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_tab_user_donation,container,false);
        user  = firebaseAuth.getCurrentUser();
        mealAdapter = new MealAdapter(mealsList,getContext());
        recyclerView = rootview.findViewById(R.id.user_donation_recyclerview);
        progressBar = rootview.findViewById(R.id.progress_circular);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        mealAdapter.setListener(new MealAdapter.myMealListener() {
            @Override
            public void onMealClicked(int position, View view) {
                Toast.makeText(getContext(), "press", Toast.LENGTH_SHORT).show();
            }
        });
        meals.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mealsList.clear();
                if (dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        final Meal mealFromdb = snapshot.getValue(Meal.class);
                        new Thread(new Runnable() {
                            public void run() {
                                if (mealFromdb.getUser_id().equals(user.getUid())){
                                    mealsList.add(mealFromdb);
                                }

                            }
                        }).start();
                    }
                    mealAdapter = new MealAdapter(mealsList,getContext());
                    recyclerView.setAdapter(mealAdapter);
                    mealAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        final ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.DOWN|ItemTouchHelper.UP, ItemTouchHelper.LEFT ) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {
                int drag_pos = dragged.getAdapterPosition();
                int target_pos = target.getAdapterPosition();
                Collections.swap(mealsList , drag_pos , target_pos);
                mealAdapter.notifyItemMoved(drag_pos,target_pos);
                return false;

            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
                AlertDialog.Builder builder  = new AlertDialog.Builder(recyclerView.getContext());
                builder.setTitle("confirm remvove").setMessage("are you sure that you to remove item?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                meals_His.setValue(mealsList.get(viewHolder.getAdapterPosition()));
                                meals.child(mealsList.get(viewHolder.getAdapterPosition()).getMeal_id()).removeValue();
                                meals_wait.child(mealsList.get(viewHolder.getAdapterPosition()).getMeal_id()).removeValue();
                                mealsList.remove(viewHolder.getAdapterPosition());
                                mealAdapter.notifyDataSetChanged();

                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mealAdapter.notifyDataSetChanged();
                    }
                }).show();



            }
        };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
            recyclerView.setAdapter(mealAdapter);
            mealAdapter.notifyDataSetChanged();

        return rootview;
    }
//    public void setPic(ImageButton imageButton1) {
//        // Get the dimensions of the View
//        int targetW = imageButton1.getWidth();
//        int targetH = imageButton1.getHeight();
//
//        // Get the dimensions of the bitmap
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//        int photoW = bmOptions.outWidth;
//        int photoH = bmOptions.outHeight;
//
//        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
//
//        // Decode the image file into a Bitmap sized to fill the View
//        bmOptions.inJustDecodeBounds = false;
//        bmOptions.inSampleSize = scaleFactor;
//        bmOptions.inPurgeable = true;
//
//        bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//        imageButton.setImageBitmap(bitmap);
//    }
}
