package com.example.sharemeals;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.example.sharemeals.adapters.ConversationAdapter;
import com.example.sharemeals.chat.ChatManager;
import com.example.sharemeals.models.ChatMessage;
import com.example.sharemeals.models.Conversation;
import com.example.sharemeals.models.Meal;
import com.example.sharemeals.search_fragment.SearchMealsFragment;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;




public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener , SearchMealsFragment.OnMealsResultListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    FloatingActionButton fab;
    CoordinatorLayout coordinatorLayout;
    FirebaseAuth firebaseAuth =  FirebaseAuth.getInstance();
    FirebaseAuth.AuthStateListener authStateListener;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    String MyPickUpFragmentTag = "My_PickUp_Fragment_Tag";
    String SearchMealsFragment = "Search_Meals_Fragment";
    String newDonationFragmentTag = "new_donation_fragment";
    String MealRequestFragmentTag = "meal_request_fragment";
    String mainPageFragmentTag = "main_page_fragment_tag";
    String SearchResultFragmentTag = "search_result_fragment_tag";
    String GoogleApiMaps = "AIzaSyB9qz0ClUsxMk9U8C8QvCJEIVnJKqw9nG4";
    public static List<ChatMessage> messagesList = new ArrayList<>();

    //messaging
    String TAG = "MyFirebaseMessaging";
    FirebaseMessaging messaging = FirebaseMessaging.getInstance();
    BroadcastReceiver receiver;
    ConversationAdapter mMessagingAdapter;
    final String AUTH_SERVER_KEY = "AAAApVe6Hds:APA91bGcRniDssb5Po44EBkd2bH0mW7nxy_X0cRAbxjWRE6FnsOyl1fWmzYTg9Z-A6mllnS7nmWgisHMly9UDfZ4pONTrnl_MOpHEMTY5JIOXE9Vez2B8N4w1reR8cM4IDO41HIxRBFH";
    //final String AUTH_SERVER_KEY = "AIzaSyCvS1lkAD_UPwUbgfAxJoF86sVpRa3Ltsk";
    final String SENDER_ID = "710141418971";
    final String PROJECT_ID = "sharemeals-2468e";
    String mThisUserKey;
    private ChatManager mChatManager;
    ImageView donateImage;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setMessaging();
        mChatManager  = new ChatManager(this,getSupportFragmentManager());
        setBroadCastReceiver();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar.setTitle(R.string.menu);
        // Initialize Places.
        Places.initialize(getApplicationContext(),GoogleApiMaps);
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);

        MainPageFragment mainPageFragment = new MainPageFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fragment,mainPageFragment,mainPageFragmentTag).commit();
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        fab = findViewById(R.id.fab);
        coordinatorLayout = findViewById(R.id.coordinator);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newDonationFragment newDonationFragment = new newDonationFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction =
                        fragmentManager.beginTransaction();
                transaction.add(R.id.fragment,newDonationFragment,newDonationFragmentTag)
                                .addToBackStack(null).commit();

            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                switch (menuItem.getItemId()){
                    case R.id.item_sign_out:
                        firebaseAuth.signOut();
                        Intent goToLogIn  = new Intent(MainActivity.this,LoginManneger.class);
                        startActivity(goToLogIn);
                        finish();
                        break;
                    case R.id.item_my_meals:
                        Intent tabActivity = new Intent(MainActivity.this , MyMealsTabLayout.class);
                        startActivity(tabActivity);
                        break;
                    case R.id.item_search_meals:
                        SearchMealsFragment searchMealsFragment = new SearchMealsFragment();
                        FragmentManager SearchMealsfragmentManager = getSupportFragmentManager();
                        FragmentTransaction SearchMealsfragmentTransaction = SearchMealsfragmentManager.beginTransaction();
                        SearchMealsfragmentTransaction.add(R.id.fragment ,searchMealsFragment,SearchMealsFragment).addToBackStack(SearchMealsFragment).commit();
                        break;
//                    case R.id.item_request_meals:
//                        final ConversationFragment mealRequestFragment = new ConversationFragment();
//                        FragmentManager mealRequestFragmentManager = getSupportFragmentManager();
//                        FragmentTransaction mealRequestFragmentTransaction = mealRequestFragmentManager.beginTransaction();
//                        mealRequestFragmentTransaction.add(R.id.fragment ,mealRequestFragment,MealRequestFragmentTag).addToBackStack(MealRequestFragmentTag).commit();
//                        break;

                    case R.id.item_chat:
                        String userId = firebaseAuth.getUid();
                        //String secondToken = "e1jeHROP6So:APA91bEjTrnBcvk2gjxn-wuBcZ16f98OAkS3PSUK4uGtTRUwm7WOQq370dU9dLYom8IPbohlPIzTrZ7pgmG49orwDHaSMN8LMig6z_ov7l4U_AhT092YxrqSP7PjGIvx1X_POtr1H_Fo";
                         String secondId = "rB1Z23zx8sWCNkKQ8wQrPyRJ3P02";  //lior
                        //String secondId = "8BvxAUlCIaYOtTGSPZxh8ep1Eng2"; //semion
                        //ma.handleIncomingMessage("1!3#"+userId.length()+":"+"abc"+userId+"Hello");

                        //mThisUserKey = "f0ATIfw1EXQ:APA91bHlNg7pGZ_rDog_7lTyAiBUyF9QAJep-2gYg7C_WUXmrf5-XQRh48U5cXX1C-nFDX-sv3ach67jY09tGgRO9bw0JzF1EZiObqBht_8s4RuBJUs6gcC59UDYsOPjamfTL-38Fve0";
                        LinkedList<Conversation> li = new LinkedList<>();
                        li.push(new Conversation(secondId,"","AViad 1","1","","",false));
                        li.push(new Conversation(secondId,"","Aviad 2","1","","",false));
                        li.push(new Conversation(secondId,"","Aviad 3","1","","",false));
//
////                        final List<Conversation> allUserConversation = new ArrayList<>();
//                        //final GenericTypeIndicator<LinkedList<Conversation>> tp = new GenericTypeIndicator<LinkedList<Conversation>>(){};
                        DatabaseReference db = FirebaseDatabase.getInstance().getReference("users").child(userId).child("conversations");
//                        //String key = db.push().getKey();
                        db.setValue(li);

                        mChatManager.showConversationsList();








//                        DatabaseReference userConversations = db.getReference("user_conversations").child(userId);
//                        db.addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                if(dataSnapshot.exists()){
//                                    //List<Conversation> all = dataSnapshot.getValue(tp);
//                                    for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
//                                        //allUserConversation.add(con);
//                                        LinkedList<Conversation> lll = new LinkedList<>(snapshot.getValue(tp));
//                                                //snapshot.getValue(tp);
//
//                                        Log.d("MyFirebaseMessaging",lll.pop()+"");
//                                    }
//
//                                }
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                            }
//                        });
//
////                        if(allUserConversation.contains(secondUserId)){
////
////                        }
//
//                        String conversationId = allUserConversation.get(0).getConversationDataId();
//                        String thisConvUserId = allUserConversation.get(0).getThisUserId();
//                        ConversationFragment conversationFragment = ConversationFragment.newInstance(conversationId,thisConvUserId);
//                        FragmentManager conversationManager = getSupportFragmentManager();
//                        FragmentTransaction ConMealsfragmentTransaction = conversationManager.beginTransaction();
//                        ConMealsfragmentTransaction.add(R.id.fragment ,conversationFragment,SearchResultFragmentTag).addToBackStack(SearchResultFragmentTag).commit();
                        break;
                }


                return false;
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments_result = getSupportFragmentManager().getFragments();
        if (fragments_result != null) {
            fragments_result.get(fragments_result.size() -1).onActivityResult(requestCode,resultCode,data);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(Gravity.START);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
                fragments.get(fragments.size() -1).onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String q = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /*private void setMessaging(){
        //FirebaseApp.initializeApp(this);
        //messaging.unsubscribeFromTopic("A");
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        mThisUserKey = token;
                        String userID = firebaseAuth.getUid();
                        if(userID != null)
                            FirebaseDatabase.getInstance().getReference().child("users").child(userID).setValue(new User("Aviad","","","","",token));
                        // Log and toast
                        // String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, token);

                    }
                });*/
//
//
//        IntentFilter filter = new IntentFilter("message_received");
//        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,filter);
//        setBroadCastReceiver();
//
//    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }


    @Override
    public void OnMealsResult(List<Meal> mealList) {
        SearchResultFragment searchResultFragment = SearchResultFragment.newinstance(mealList);
        FragmentManager SearchResultfragmentManager = getSupportFragmentManager();
        FragmentTransaction SearchMealsfragmentTransaction = SearchResultfragmentManager.beginTransaction();
        SearchMealsfragmentTransaction.add(R.id.fragment ,searchResultFragment,SearchResultFragmentTag).addToBackStack(SearchResultFragmentTag).commit();
    }


//    public void send(String textToSend){
//        final JSONObject rootObject  = new JSONObject();
//        try {
//            mThisUserKey = "f0ATIfw1EXQ:APA91bHlNg7pGZ_rDog_7lTyAiBUyF9QAJep-2gYg7C_WUXmrf5-XQRh48U5cXX1C-nFDX-sv3ach67jY09tGgRO9bw0JzF1EZiObqBht_8s4RuBJUs6gcC59UDYsOPjamfTL-38Fve0";
//   //         mThisUserKey = "e1jeHROP6So:APA91bEjTrnBcvk2gjxn-wuBcZ16f98OAkS3PSUK4uGtTRUwm7WOQq370dU9dLYom8IPbohlPIzTrZ7pgmG49orwDHaSMN8LMig6z_ov7l4U_AhT092YxrqSP7PjGIvx1X_POtr1H_Fo";
//            rootObject.put("to", mThisUserKey);
//            //rootObject.put("to", "/topics/A");
//
//            rootObject.put("data",new JSONObject().put("message",textToSend));
////            rootObject.put("data",new JSONObject().put("message","aviad test"));
//
//            String url = "https://fcm.googleapis.com/fcm/send";
//            RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
//            StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    Log.d(TAG, "RESPONSE: "+response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(MainActivity.this, "ere", Toast.LENGTH_SHORT).show();
//                }
//            }) {
//
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    Map<String,String> headers = new HashMap<>();
//                    headers.put("Content-Type","application/json");
//                    headers.put("Authorization","key="+AUTH_SERVER_KEY);
//                    //headers.put(PROJECT_ID,SENDER_ID);
//                    return headers;
//                }
//
//                @Override
//                public byte[] getBody() throws AuthFailureError {
//                    return rootObject.toString().getBytes();
//                }
//            };
//            queue.add(request);
//            queue.start();
//
//
//        }catch (JSONException ex) {
//            ex.printStackTrace();
//        }
//
//
//    }


    public void setAdapter(ConversationAdapter messagingAdapter){
        mMessagingAdapter=messagingAdapter;
    }

    public void removeAdapter(){
        mMessagingAdapter=null;
    }

    private void setBroadCastReceiver() {

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //messagesList.add(new ChatMessage(intent.getStringExtra("message"), false));
                if (mMessagingAdapter != null)
                    mMessagingAdapter.notifyDataSetChanged();
                if(mChatManager != null) {
                    mChatManager.handleIncomingMessage(intent.getStringExtra("message"));
//                    Toast.makeText(context,intent.getStringExtra("message"),Toast.LENGTH_LONG).show();
                }

                Log.d("MyFirebaseMessaging","BROADCAST Message:" +intent.getStringExtra("message"));
            }
        };
        Log.d("MyFirebaseMessaging","BROADCAST initialize");
        IntentFilter filter = new IntentFilter("message_received");
        //LocalBroadcastManager.getInstance(this).
        registerReceiver(receiver,filter);
    }
}


