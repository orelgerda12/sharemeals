package com.example.sharemeals;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class DBListener {
    //test

    public interface OnDBChangeListener {
        void onDBChangeListener(DataSnapshot dataSnapshot);
    }


    private OnDBChangeListener mListener;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals");


    public DBListener(OnDBChangeListener listener, String mainDBRoot, String child){
        DatabaseReference meals = pickupMeals.getReference(mainDBRoot).child(child);
        mListener = listener;
        listenToDB();
    }

    public DBListener(OnDBChangeListener listener) {
        DatabaseReference meals = pickupMeals.getReference("meals");
        mListener = listener;
        listenToDB();
    }


    private void listenToDB() {

        meals.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                //mealsList.clear();
//                Log.d("onDataChange","fun called");
//                if (dataSnapshot.exists()){
//                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
//                        String meal = snapshot.getKey();
//                        //mealsList.add(meal);
//                        Log.d("onDataChange","data snapshot noy null = "+meal);
//                    }
//                    //adapter.notifyDataSetChanged();
//                }
//            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                if (dataSnapshot.exists())
                    //Log.d("onDataChange", "onChildAdded:" + dataSnapshot.getKey() + dataSnapshot.getValue());
                    mListener.onDBChangeListener(dataSnapshot);
                // A new comment has been added, add it to the displayed list


                // ...
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d("onDataChange", "onChildChanged:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so displayed the changed comment.

                String commentKey = dataSnapshot.getKey();

                // ...
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("onDataChange", "onChildRemoved:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so remove it.
                String commentKey = dataSnapshot.getKey();

                // ...
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d("onDataChange", "onChildMoved:" + dataSnapshot.getKey());

                // A comment has changed position, use the key to determine if we are
                // displaying this comment and if so move it.

                String commentKey = dataSnapshot.getKey();

                // ...
            }


        });
    }

}