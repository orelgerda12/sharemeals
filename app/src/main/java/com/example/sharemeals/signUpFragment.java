package com.example.sharemeals;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.sharemeals.models.User;

import java.util.Locale;
import java.util.regex.Pattern;

public class signUpFragment extends Fragment {
    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
//                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{8,}" +               //at least 4 characters
                    "$");

    //TODO : make sure that we have fullname and username varlible
    private TextInputLayout textInputEmail;
    private TextInputLayout textInputUsername;
    private TextInputLayout textInputFullname;
    private TextInputLayout textInputPassword;
    boolean IsRestorant = false;
    AutoCompleteTextView CitiesautoCompleteTextView;
    private TextInputLayout textInputAdress;
    RadioGroup radioGroup;

    Button sign_up_btn;


    interface OnSignUpFragmentListener{
        void onSignUp(User newUser);
    }
    OnSignUpFragmentListener callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (OnSignUpFragmentListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException("the activity isnt implemen OnSignUpFragmentListener");
        }
    }

    public static signUpFragment newInstance() {
        signUpFragment fragment = new signUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview;
        rootview = inflater.inflate(R.layout.sing_up_layout,container,false);
        String[] CityList = getResources().getStringArray(R.array.cities_heb);
        textInputUsername = rootview.findViewById(R.id.text_input_username);
        textInputEmail = rootview.findViewById(R.id.text_input_email);
        textInputPassword = rootview.findViewById(R.id.text_input_password);
        textInputFullname = rootview.findViewById(R.id.text_input_fullname);
        CitiesautoCompleteTextView = rootview.findViewById(R.id.text_input_city);
        radioGroup = rootview.findViewById(R.id.radio_restorant);
        ArrayAdapter<String> CitiesautoCompleteaAdapter;
        CitiesautoCompleteaAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, CityList);
        CitiesautoCompleteTextView.setAdapter(CitiesautoCompleteaAdapter);
        textInputAdress = rootview.findViewById(R.id.text_input_addres);
        sign_up_btn = rootview.findViewById(R.id.register);
        final User user = new User();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radio_restorant){
                    IsRestorant = true;
                }
            }
        });


        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  if (!validateEmail() | !validateUsername() | !validatePassword()) {
                          return;
                }
                    user.setMail(textInputEmail.getEditText().getText().toString());
                    user.setUsername(textInputUsername.getEditText().getText().toString());
                    user.setPassword(textInputPassword.getEditText().getText().toString());
                    user.setCity(CitiesautoCompleteTextView.getText().toString());
                    user.setFirstName(textInputFullname.getEditText().getText().toString());
                    user.setRestaurant(IsRestorant);
                    callback.onSignUp(user);
            }});

        return rootview;

    }
    private boolean validateEmail() {
        String emailInput = textInputEmail.getEditText().getText().toString().trim();

        if (emailInput.isEmpty()) {
            textInputEmail.setError(getString(R.string.can_not_be_empty));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            textInputEmail.setError(getString(R.string.valid_email));
            return false;
        } else {
            textInputEmail.setError(null);
            return true;
        }
    }

    private boolean validateUsername() {
        String usernameInput = textInputFullname.getEditText().getText().toString().trim();

        if (usernameInput.isEmpty()) {
            textInputFullname.setError(getString(R.string.can_not_be_empty));
            return false;
        } else if (usernameInput.length() > 15) {
            textInputUsername.setError("Username too long");
            return false;
        } else {
            textInputUsername.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = textInputPassword.getEditText().getText().toString().trim();

        if (passwordInput.isEmpty()) {
            textInputPassword.setError(getString(R.string.can_not_be_empty));
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            textInputPassword.setError("Password too weak");
            return false;
        } else {
            textInputPassword.setError(null);
            return true;
        }
    }




}
