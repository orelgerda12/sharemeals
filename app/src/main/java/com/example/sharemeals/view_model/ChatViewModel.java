package com.example.sharemeals.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.text.BoringLayout;

import com.example.sharemeals.chat.ChatManager;
import com.example.sharemeals.models.ChatMessage;
import com.example.sharemeals.models.ChatMessageWrapper;
import com.example.sharemeals.models.Conversation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class ChatViewModel extends ViewModel {

    MutableLiveData<LinkedList<Conversation>> conversationsListUpdate = new MutableLiveData<>();
    MutableLiveData<LinkedList<ChatMessage>> messages = new MutableLiveData<>();
    MutableLiveData<ChatMessageWrapper> incomingMessage = new MutableLiveData<>();
    MutableLiveData<String> updateAdapter = new MutableLiveData<>();
    MutableLiveData<String> updateAdapterConversationList = new MutableLiveData<>();

    final GenericTypeIndicator<ArrayList<Conversation>> tp1 = new GenericTypeIndicator<ArrayList<Conversation>>(){};
    final GenericTypeIndicator<ArrayList<ChatMessage>> tp2 = new GenericTypeIndicator<ArrayList<ChatMessage>>(){};

    public MutableLiveData<LinkedList<Conversation>> getConversationsListUpdated(String uid) {


        //MUST CHECK***************************************ChatManager disable fetch.... and enable this

        /*FirebaseDatabase.getInstance().getReference("users").child(uid).child("conversations")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()) {
                            ArrayList<Conversation> conversationsList = dataSnapshot.getValue(tp1);
                            //for(DataSnapshot snapshot:dataSnapshot.getChildren()) {


                           // }
                            conversationsListUpdate.setValue(new LinkedList<>(conversationsList));
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });*/

        return conversationsListUpdate;
    }


    public MutableLiveData<LinkedList<ChatMessage>> getMessages(String uid,String secondUserId) {

        FirebaseDatabase.getInstance().getReference("users").child(uid).child("conversationData").child(secondUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()) {
                            ArrayList<ChatMessage> messagesList = dataSnapshot.getValue(tp2);
                            //for(DataSnapshot snapshot:dataSnapshot.getChildren()) {


                            // }
                            messages.setValue(new LinkedList<>(messagesList));
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        return messages;
    }


    public MutableLiveData<ChatMessageWrapper> getNewMessages() {

        return incomingMessage;
    }


    public MutableLiveData<String> getUpdateAdapter() {

        return updateAdapter;
    }

    public MutableLiveData<String> getUpdateAdapterConversationList() {

        return updateAdapterConversationList;
    }

    public void selectSendNewMessage(ChatMessageWrapper item) {
        incomingMessage.setValue(item);
    }

    public void selectConversationsList(LinkedList<Conversation> list){
        conversationsListUpdate.setValue(list);
    }

    public void selectMessagesList(LinkedList<ChatMessage> list){
        messages.setValue(list);
    }

    public void selectUpdateAdapter(String item){
        updateAdapter.setValue(item);
    }

    public void selectUpdateAdapterConversationList(String item){
        updateAdapterConversationList.setValue(item);
    }
}

