package com.example.sharemeals.adapters;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sharemeals.R;
import com.example.sharemeals.models.ChatMessage;
import com.example.sharemeals.models.Meal;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ConversationMessageHolder> {
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals");
    private List<ChatMessage> messagesList;
    private int mScreenWidth;
    public ConversationAdapter(List<ChatMessage> messagesList){
        this.messagesList = messagesList;

    }
    public class ConversationMessageHolder extends RecyclerView.ViewHolder{
        TextView name , kosher , date;
        RelativeLayout holderView;
        LinearLayout lin;
        public ConversationMessageHolder(View itemView){
            super(itemView);
            holderView = (RelativeLayout)itemView;
            lin = itemView.findViewById(R.id.lin);
            name = itemView.findViewById(R.id.text_view_message);
//            date = itemView.findViewById(R.id.meal_date);
//            kosher = itemView.findViewById(R.id.meal_kosher);

        }

    }

    @NonNull
    @Override
    public ConversationAdapter.ConversationMessageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_message_layout,viewGroup,false);
        ConversationAdapter.ConversationMessageHolder messageViewHolder = new ConversationAdapter.ConversationMessageHolder(view);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)messageViewHolder.lin.getLayoutParams();
//        ChatMessage message = messagesList.get(i);
//        if(message.isSent()){
//            //messageViewHolder.lin.
//
//            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            //params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
//
//
//
//        }else{
//            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        }

        messageViewHolder.lin.setLayoutParams(params); //causes layout update
        return messageViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationAdapter.ConversationMessageHolder messageViewHolder, int i) {
        int index = messagesList.size()-1-i;
        ChatMessage message = messagesList.get(index);
        messageViewHolder.name.setText(messagesList.get(index).getText());
//        //mealViewHolder.kosher.setText(model.getKosher());
//        mealViewHolder.name.setText(model.getName());
//        mealViewHolder.date.setText(model.getLast_time_for_pick());
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)messageViewHolder.lin.getLayoutParams();
//        if(message.isSent()){
//            //messageViewHolder.lin.
//
//            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            //params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
//
//
//
//        }else{
//            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        }
//
//        messageViewHolder.lin.setLayoutParams(params); //causes layout update
    }
    @Override
    public int getItemCount() {
        return messagesList.size();
    }

}


