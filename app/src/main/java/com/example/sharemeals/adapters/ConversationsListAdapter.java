package com.example.sharemeals.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sharemeals.R;
import com.example.sharemeals.chat.ChatManager;
import com.example.sharemeals.models.ChatMessage;
import com.example.sharemeals.models.Conversation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.LinkedList;
import java.util.List;

public class ConversationsListAdapter  extends RecyclerView.Adapter<ConversationsListAdapter.ConversationHolder> {
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals");
    private LinkedList<Conversation> messagesList;
    private int mScreenWidth;
    private OnConversationClickListener mListener;
    public ConversationsListAdapter(LinkedList<Conversation> messagesList){
        this.messagesList = messagesList;

    }

    public interface OnConversationClickListener{
        void onConversationClick(int i);
    }


    public class ConversationHolder extends RecyclerView.ViewHolder{
        TextView name , lastMessage , date,status;
        RelativeLayout holderView;
        public ConversationHolder(View itemView){
            super(itemView);
            holderView = itemView.findViewById(R.id.conversation_item_relative_layout);
            name = itemView.findViewById(R.id.text_view_user_name);
            date = itemView.findViewById(R.id.text_view_date);
            lastMessage = itemView.findViewById(R.id.text_view_last_message);
            status = itemView.findViewById(R.id.text_view_status);

            holderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onConversationClick(getAdapterPosition());
                }
            });
        }
    }

    public void setListener(OnConversationClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ConversationsListAdapter.ConversationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_item,viewGroup,false);
        ConversationsListAdapter.ConversationHolder conversationViewHolder = new ConversationsListAdapter.ConversationHolder(view);
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)messageViewHolder.lin.getLayoutParams();
//        ChatMessage message = messagesList.get(i);
//        if(message.isSent()){
//            //messageViewHolder.lin.
//
//            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            //params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
//
//
//
//        }else{
//            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        }

//        messageViewHolder.lin.setLayoutParams(params); //causes layout update
        return conversationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationsListAdapter.ConversationHolder messageViewHolder, int i) {
        Conversation conversation = ChatManager.allUserConversations.get(i);
        messageViewHolder.name.setText(conversation.getSecondUserName());
//        //mealViewHolder.kosher.setText(model.getKosher());
//        mealViewHolder.name.setText(model.getName());
//        mealViewHolder.date.setText(model.getLast_time_for_pick());
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)messageViewHolder.lin.getLayoutParams();
//        if(message.isSent()){
//            //messageViewHolder.lin.
//
//            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            //params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
//
//
//
//        }else{
//            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        }
//
//        messageViewHolder.lin.setLayoutParams(params); //causes layout update
    }
    @Override
    public int getItemCount() {
        return ChatManager.allUserConversations.size();
    }

}
