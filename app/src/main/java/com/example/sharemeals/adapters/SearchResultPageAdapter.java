package com.example.sharemeals.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharemeals.R;
import com.example.sharemeals.models.Meal;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class SearchResultPageAdapter extends PagerAdapter {
    Context context;
    List<Meal> mealList;
    LayoutInflater inflater;
    private FirebaseStorage Meal_images = FirebaseStorage.getInstance();
    private StorageReference Meal_images_Ref = Meal_images.getReference();
    private ImageView meal_image , glutten_free , lactos_free , penuts_free , kosher;
    private TextView meal_Area , meal_name , meal_city ,meal_address , last_time_pickup;
    private ProgressBar progressBar;
    public SearchResultPageAdapter(List<Meal> mealList ,Context context ){
        this.mealList = mealList;
        this.context = context;
        inflater = LayoutInflater.from(context);

    }
    public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        private int currentPage;

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
        }

        public final int getCurrentPage() {
            return currentPage;
        }
    }
//    myViewpagerListener CallBackViewpager;
////    public interface myViewpagerListener{
////        void OnpickupClicked(int position);
////    }
////    public void setListener(myViewpagerListener CallBackViewpager){
////        this.CallBackViewpager = CallBackViewpager;
////    }
    @Override
    public int getCount() {
        return mealList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o ;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        (container).removeView((View)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Meal model = mealList.get(position);
        View view = inflater.inflate(R.layout.search_results_item2, container , false);
        // view
        meal_image = view.findViewById(R.id.search_result_meal_image);
        meal_Area = view.findViewById(R.id.search_result_meal_area_model);
        meal_name = view.findViewById(R.id.search_result_meal_name_model);
        meal_city = view.findViewById(R.id.search_result_meal_city_model);
        meal_address = view.findViewById(R.id.search_result_meal_address_model);
        glutten_free = view.findViewById(R.id.search_result_glutten_free);
        lactos_free = view.findViewById(R.id.search_result_lactos_free);
        penuts_free = view.findViewById(R.id.search_result_penuts_free);
        last_time_pickup = view.findViewById(R.id.search_result_meal_last_time_model);
//        progressBar = view.findViewById(R.id.search_result_progressbar);
        kosher = view.findViewById(R.id.search_result_kosher_icon);
//        FloatingActionButton fab = view.findViewById(R.id.search_fab_pickme);
        //load data
        LoadImage(model);
        meal_name.setText(model.getName());
        meal_Area.setText(model.getArea());
        meal_city.setText(model.getCity());
        meal_address.setText(model.getAddress());
        last_time_pickup.setText(model.getLast_time_for_pick());
        if(model.getElrg_nuts()){
            penuts_free.setVisibility(View.VISIBLE);
        }
        if(model.getElrg_glutten()){
            glutten_free.setVisibility(View.VISIBLE);
        }
        if(model.getElrg_lactos()){
            lactos_free.setVisibility(View.VISIBLE);
        }
        if (model.getKosher()){
            kosher.setVisibility(View.VISIBLE);
        }
        container.addView(view);
        return view;
    }

    public void LoadImage(Meal model){
        StorageReference ref = Meal_images_Ref.child("meals_images/" + model.getImage_link());
        final long ONE_MEGABYTE = 1024 * 1024;
        ref.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                // Data for "images/island.jpg" is returns, use this as needed
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//                progressBar.setVisibility(View.GONE);
                meal_image.setImageBitmap(bitmap);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
