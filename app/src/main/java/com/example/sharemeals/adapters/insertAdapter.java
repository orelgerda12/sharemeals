package com.example.sharemeals.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sharemeals.R;

public class insertAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    public insertAdapter(Context context){
        this.context=context;
    }
    //list of image
    public int [] img_list= {R.drawable.insert_background1,R.drawable.insert2 };
    @Override
    public int getCount() {
        return img_list.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view == o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.insert_slid_image,container,false);
        LinearLayout linearLayout =  view.findViewById(R.id.line_layout);
        ImageView imageView = view.findViewById(R.id.insert_image_view);
        imageView.setImageResource(img_list[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
