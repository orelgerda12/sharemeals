package com.example.sharemeals.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.sharemeals.MainActivity;
import com.example.sharemeals.R;
import com.example.sharemeals.models.Meal;
import com.example.sharemeals.models.User;
import com.example.sharemeals.search_fragment.CategoryAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class MealAdapter extends RecyclerView.Adapter<MealAdapter.MealViewHolder> {
    private FirebaseStorage Meal_images = FirebaseStorage.getInstance();
    StorageReference Meal_images_Ref = Meal_images.getReference();
    private List<Meal> MealModel;
    android.content.Context context;
    myMealListener listener;
    public interface myMealListener{
        void onMealClicked(int position , View view);
    }
    public void setListener(myMealListener listener){
        this.listener = listener;
    }

    public MealAdapter(List<Meal> MealModel , Context context){
        this.MealModel = MealModel;
        this.context = context;
    }

    public MealAdapter() {
    }

    public class MealViewHolder extends RecyclerView.ViewHolder{
        TextView name , location , status , last_time_pick;
        ImageView imageView;
        public MealViewHolder(View itemView){
            super(itemView);
            name = itemView.findViewById(R.id.meal_name_tab);
            imageView = itemView.findViewById(R.id.meal_image_tab);
            location = itemView.findViewById(R.id.meal_location_tab);
            status = itemView.findViewById(R.id.meal_status_tab);
            last_time_pick = itemView.findViewById(R.id.meal_last_time_tab);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null)
                        listener.onMealClicked(getAdapterPosition(),v);
                }

            });
        }

    }
    @NonNull
    @Override
    public MealViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.my_pickup_item,viewGroup,false);
        MealViewHolder mealViewHolder = new MealViewHolder(view);
        return mealViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MealViewHolder mealViewHolder, int i) {
        Meal model = MealModel.get(i);
        mealViewHolder.name.setText(model.getName());
        mealViewHolder.location.setText(context.getString(R.string.result_meal_address)+model.getCity() + " " + model.getAddress());
        mealViewHolder.status.setText(context.getString(R.string.result_meal_status)+" " + model.getStatus());
        mealViewHolder.last_time_pick.setText(model.getLast_time_for_pick());
        //download image
        StorageReference ref = Meal_images_Ref.child("meals_images/" + model.getImage_link());
        final long ONE_MEGABYTE = 512 * 512;
        ref.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                // Data for "images/island.jpg" is returns, use this as needed
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                bitmap.compress(Bitmap.CompressFormat.JPEG,30,outputStream);
                byte[] byteArray = outputStream.toByteArray();
                Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
                mealViewHolder.imageView.setImageBitmap(compressedBitmap);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
//        mealViewHolder.name.setText(model.getName());
//        mealViewHolder.date.setText(model.getLast_time_for_pick());
    }
    @Override
    public int getItemCount() {
        return MealModel.size();
    }

}
