package com.example.sharemeals.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sharemeals.FragmentTabUserDonation;
import com.example.sharemeals.FragmentTabUserPickup;

public class MyMealsTabLayoutAdapter extends FragmentStatePagerAdapter {
    int tabcount;

    public MyMealsTabLayoutAdapter(FragmentManager fm, int tabcount) {
        super(fm);
        this.tabcount = tabcount;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                FragmentTabUserDonation fragmentTabUserDonation = new FragmentTabUserDonation();
                return  fragmentTabUserDonation;
            case 1:

            FragmentTabUserPickup fragmentTabUserPickup = new FragmentTabUserPickup();
            return  fragmentTabUserPickup;
            default:return null;
        }

    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
