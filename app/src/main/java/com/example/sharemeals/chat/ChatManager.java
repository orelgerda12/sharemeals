package com.example.sharemeals.chat;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sharemeals.ConversationFragment;
import com.example.sharemeals.MainActivity;
import com.example.sharemeals.R;
import com.example.sharemeals.models.ChatMessage;
import com.example.sharemeals.models.ChatMessageWrapper;
import com.example.sharemeals.models.Conversation;
import com.example.sharemeals.models.User;
import com.example.sharemeals.view_model.ChatViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.common.collect.LinkedListMultimap;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;




public class ChatManager implements ConversationsListFragment.OnItemClickedListener,ConversationFragment.OnSendNewMessageListener {

    String TAG = "MyFirebaseMessaging";
    final String SENDER_ID = "710141418971";
    final String PROJECT_ID = "sharemeals-2468e";
    private final String AUTH_SERVER_KEY = "AAAApVe6Hds:APA91bGcRniDssb5Po44EBkd2bH0mW7nxy_X0cRAbxjWRE6FnsOyl1fWmzYTg9Z-A6mllnS7nmWgisHMly9UDfZ4pONTrnl_MOpHEMTY5JIOXE9Vez2B8N4w1reR8cM4IDO41HIxRBFH";
    public static LinkedList<Conversation> allUserConversations = new LinkedList<>();
    private FragmentManager mRfagmentManager;
    private static User secondUserTemp;
    private static String thisUserToken;
    private static FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    public static String thisUserId = firebaseAuth.getUid();
    private String messageToSendPrefix;
    private AppCompatActivity mActivity;
    public ChatViewModel mChatViewModel;
    public static HashMap<String, LinkedList<ChatMessage>> messagesMap = new HashMap<>();
    private String destinationToken = "f0ATIfw1EXQ:APA91bHlNg7pGZ_rDog_7lTyAiBUyF9QAJep-2gYg7C_WUXmrf5-XQRh48U5cXX1C-nFDX-sv3ach67jY09tGgRO9bw0JzF1EZiObqBht_8s4RuBJUs6gcC59UDYsOPjamfTL-38Fve0";

    public ChatManager() {
    }

    public ChatManager(AppCompatActivity activity, FragmentManager mRfagmentManager) {
        if (thisUserId == null) {
            Toast.makeText(activity, "Failed to get user id (ChatManager)", Toast.LENGTH_LONG).show();
            return;
        }

        this.mRfagmentManager = mRfagmentManager;
        mActivity = activity;
        getThisUserToken();
        fetchAllPreviousConversations();
        initializeViewModel();
    }

    private void fetchAllPreviousConversations() {
        final GenericTypeIndicator<ArrayList<Conversation>> tp = new GenericTypeIndicator<ArrayList<Conversation>>() {
        };
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference userConversations = db.getReference("users").child(thisUserId).child("conversations");
        userConversations.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    //allUserConversations = new LinkedList<>(dataSnapshot.getValue(tp));
                    List<Conversation> all = dataSnapshot.getValue(tp);
                    for (Conversation con : all) {
                        allUserConversations.add(con);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void initializeViewModel() {
        mChatViewModel = ViewModelProviders.of(mActivity).get(ChatViewModel.class);
        /*mChatViewModel.getConversationsListUpdated(thisUserId).observe(mActivity, new Observer<LinkedList<Conversation>>() {
            @Override
            public void onChanged(@Nullable LinkedList<Conversation> conversations) {
                allUserConversations = conversations;
            }
        });*/

        mChatViewModel.getNewMessages().observe(mActivity, new Observer<ChatMessageWrapper>() {
            @Override
            public void onChanged(@Nullable ChatMessageWrapper message) {
                messagesMap.get(message.destinationUserId).offerLast(message.message);
                //mChatViewModel.selectUpdateAdapter("");
                send(message.destinationUserToken, messageToSendPrefix + message.message.getText());
            }
        });
    }

    //                  i                     z yyyyyyyyyyy ttttttttttttt
    //{thisUserIdLength}#{thisUserTokenLength}:{thisUserId}{thisUserToken}{message}
    public void handleIncomingMessage(String message) {
        //Toast.makeText(mActivity,message,Toast.LENGTH_LONG).show();
        int i = message.indexOf("#");
        int y, t = 0;
        String userToken = null;
        String incomingMessage = null;
        String secondUserId = null;
        String userIdLength = message.substring(0, i);
        int z = message.indexOf(":");
        i++;
        String userTokenLength = message.substring(i, z);

        Log.d("MyFirebaseMessaging", "userIdLength= " + userIdLength);
        Log.d("MyFirebaseMessaging", "userTokenLength= " + userTokenLength);
        try {
            y = Integer.parseInt(userIdLength);
            t = Integer.parseInt(userTokenLength);
            z++;
            y += z;
            secondUserId = message.substring(z, y);
            t += y;
            userToken = message.substring(y, t);
            incomingMessage = message.substring(t);
            Log.d("MyFirebaseMessaging", "y= " + y);
            Log.d("MyFirebaseMessaging", "userId= " + secondUserId);
            Log.d("MyFirebaseMessaging", "senderToken= " + userToken);
            Log.d("MyFirebaseMessaging", "message= " + incomingMessage);


        } catch (Exception e) {
            Log.d("MyFirebaseMessaging", e.toString());
        }
        ChatMessage incoming = new ChatMessage(incomingMessage, "", "", secondUserId, false);
        if (messagesMap.containsKey(secondUserId)) { //check if is it the the first conversation with the sender
            //if not
            messagesMap.get(secondUserId).offerLast(incoming);
            mChatViewModel.selectUpdateAdapter("");
        } else {
            //if yes
            //Toast.makeText(mActivity,secondUserId+"\n"+thisUserId+"\n"+incomingMessage+"\n"+userToken,Toast.LENGTH_LONG).show();
            startNewConversatio(thisUserId, secondUserId, true);    //creating new conversation in dedicated list
//            LinkedList<ChatMessage> link = new LinkedList<>();
//            link.offerLast(incoming);
            getPreviousMessages(secondUserId,incoming);
            //messagesMap.put(secondUserId, link);
        }
    }

//    //                         i                   zyyyyyyyyyyyyyyyyyyyyyyy  tttttttttttt
//    //{thisUserId}{converLength}#{senderTokenLength}:{incomingConversationId}{senderUserId}{message}
//    public void handleIncomingMessage(String message){
//        int i = message.indexOf("#");
//        int y ,t = 0;
//        String thisUserId = message.substring(0,1);
//        String converLength = message.substring(2,i);
//        int z = message.indexOf(":");
//        i++;
//        String senderUserIdLength = message.substring(i,z);
//
//        Log.d("MyFirebaseMessaging", "thisUserId= "+thisUserId);
//        Log.d("MyFirebaseMessaging", "converLength= "+converLength);
//        Log.d("MyFirebaseMessaging", "senderTokenLength= "+senderUserIdLength);
//        try {
//            y = Integer.parseInt(converLength);
//            t = Integer.parseInt(senderUserIdLength);
//            z++;
//            y+=z;
//            String incomingConversationId  = message.substring(z,y);
//            t+=y;
//            String senderUserId = message.substring(y,t);
//            String incomingMessage = message.substring(t);
//            Log.d("MyFirebaseMessaging", "y= "+y);
//            Log.d("MyFirebaseMessaging", "incomingConversationId= "+incomingConversationId);
//            Log.d("MyFirebaseMessaging", "senderToken= "+senderUserId);
//            Log.d("MyFirebaseMessaging", "message= "+incomingMessage);
////            if(isThisNewConversation(incomingConversationId)){   //new conversation,update this user conversationsDB
////                Conversation conv = new Conversation(incomingMessage,null,incomingConversationId,"2");
////
////                //LinkedList<String> li = new LinkedList<>();
////
////            }
//        }catch (Exception e) {
//            Log.d("MyFirebaseMessaging", e.toString());
//        }
//    }

    //check if this the first conversation ever with the second user
    private int isThisNewConversation(String secondUserId) {
        for (int i = 0; i < allUserConversations.size(); i++) {
            if (allUserConversations.get(i).getSecondUserID().equals(secondUserId))
                return i;
        }
        return -1;
    }


    public void startNewConversatio(final String thisUserId, final String secondUsrId, final boolean isIncomingMessage) {
        final FirebaseDatabase db = FirebaseDatabase.getInstance();//

        DatabaseReference ref = db.getReference().child("users").child(secondUsrId).child("info"); //fetch the token to send the message to
        final GenericTypeIndicator<LinkedList<Conversation>> tp = new GenericTypeIndicator<LinkedList<Conversation>>() {
        };

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    secondUserTemp = dataSnapshot.getValue(User.class);
                    Log.d("MyFirebaseMessaging", secondUserTemp.getUsername());
                    int secondUserIndex = isThisNewConversation(secondUsrId);
                    String destinationToken;
                    Conversation conversation = new Conversation(secondUsrId, secondUserTemp.getMessageToken(), secondUserTemp.getUsername(), "1", "", "", false);

                    if (secondUserIndex != -1) {
                        Toast.makeText(mActivity, secondUserIndex + " removed", Toast.LENGTH_LONG).show();
                        allUserConversations.remove(secondUserIndex);
                    }
                    allUserConversations.offerFirst(conversation);
                    LinkedList<Conversation> all = new LinkedList<>();
                    for (Conversation con : allUserConversations) {
                        all.add(new Conversation(con.getSecondUserID(), con.getSecondUserToken(), con.getSecondUserName(), con.getThisUserId(), con.getLastMessage(), con.getLastMessageDate(), con.isStatus()));
                    }
                    mChatViewModel.selectConversationsList(all);
//                                    Toast.makeText(mActivity,allUserConversations.get(0).getSecondUserToken(),Toast.LENGTH_LONG).show();
                    FirebaseDatabase.getInstance().getReference("users").child(thisUserId).child("conversations").setValue(allUserConversations);

                    //send(destinationToken,messageToSendPrefix);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//            db.child("conversations").child(userId).child(secondUsrId);
//            db.child("conversations").child(userId).child(secondUsrId);
//
////        String key = db.push().getKey();
//        ChatMessage mes = new ChatMessage("","Hello","","1");
//        db.setValue(mes);
//        act.send();
    }

    private void showChatFragment(String secondUserId, String secondUserToken, String secondUserName) {
        //String conversationId = allUserConversations.get(0).getConversationDataId();
        //String thisConvUserId = allUserConversations.get(0).getThisUserId();
        ConversationFragment conversationFragment = ConversationFragment.newInstance(secondUserId, secondUserToken, secondUserName);
        //FragmentManager conversationManager =  getSupportFragmentManager();
        FragmentTransaction ConMealsfragmentTransaction = mRfagmentManager.beginTransaction();
        ConMealsfragmentTransaction.add(R.id.fragment, conversationFragment).commit();

    }

    //                         i                   zyyyyyyyyyyyyyyyyyyyyyyy  tttttttttttt
    //{thisUserId}{converLength}#{senderTokenLength}:{incomingConversationId}{senderUserId}{message}
    private void buildMessageToSendPrefix() {
        messageToSendPrefix = thisUserId.length() + "#" + thisUserToken.length() + ":" + thisUserId + thisUserToken;
    }


    private void getThisUserToken() {
        //FirebaseApp.initializeApp(this);
        //messaging.unsubscribeFromTopic("A");
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("MyFirebaseMessaging", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        thisUserToken = task.getResult().getToken();
                        if (thisUserId != null)
                            FirebaseDatabase.getInstance().getReference().child("users").child(thisUserId).child("info").setValue(new User(firebaseAuth.getCurrentUser().getEmail(), "", "", "", "", thisUserToken));
                        Log.d("MyFirebaseMessaging", thisUserToken);
                        buildMessageToSendPrefix();
                    }
                });
    }


    public void send(String destination, String textToSend) {
        sendMessageToDestination(destination, textToSend);

    }

    public void sendMessageToDestination(String destination, String textToSend) {
        final JSONObject rootObject = new JSONObject();
        Log.d("MyFirebaseMessaging", "destination: " + destination);
        Log.d("MyFirebaseMessaging", "textToSend: " + textToSend);
        try {
            //mThisUserKey = "f0ATIfw1EXQ:APA91bHlNg7pGZ_rDog_7lTyAiBUyF9QAJep-2gYg7C_WUXmrf5-XQRh48U5cXX1C-nFDX-sv3ach67jY09tGgRO9bw0JzF1EZiObqBht_8s4RuBJUs6gcC59UDYsOPjamfTL-38Fve0";
            //         mThisUserKey = "e1jeHROP6So:APA91bEjTrnBcvk2gjxn-wuBcZ16f98OAkS3PSUK4uGtTRUwm7WOQq370dU9dLYom8IPbohlPIzTrZ7pgmG49orwDHaSMN8LMig6z_ov7l4U_AhT092YxrqSP7PjGIvx1X_POtr1H_Fo";
            rootObject.put("to", destination);
            //rootObject.put("to", "/topics/A");

            rootObject.put("data", new JSONObject().put("message", textToSend));
//            rootObject.put("data",new JSONObject().put("message","aviad test"));

            String url = "https://fcm.googleapis.com/fcm/send";
            RequestQueue queue = Volley.newRequestQueue(mActivity);
            StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("MyFirebaseMessaging", "RESPONSE: " + response);
                    mChatViewModel.selectUpdateAdapter("");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("MyFirebaseMessaging", "ERROR_RESPONSE: " + error.toString());
                    //Toast.makeText(MainActivity.this, "ere", Toast.LENGTH_SHORT).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "key=" + AUTH_SERVER_KEY);
                    //headers.put(PROJECT_ID,SENDER_ID);
                    return headers;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    return rootObject.toString().getBytes();
                }
            };
            queue.add(request);
            queue.start();


        } catch (JSONException ex) {
            ex.printStackTrace();
        }


    }


    public void showConversationsList() {
        ConversationsListFragment conversationFragment = ConversationsListFragment.newInstance();
        conversationFragment.setListener(this);
        //FragmentManager conversationManager =  getSupportFragmentManager();
        FragmentTransaction ConMealsfragmentTransaction = mRfagmentManager.beginTransaction();
        ConMealsfragmentTransaction.add(R.id.fragment, conversationFragment).commit();

    }

    //on conversation click on the conversation list fragment
    @Override
    public void onItemClick(int i) {
        String secondUserId = allUserConversations.get(i).getSecondUserID();
        String secondUserToken = allUserConversations.get(i).getSecondUserToken();
        String secondUserName = allUserConversations.get(i).getSecondUserName();
        if (!messagesMap.containsKey(secondUserId)) {
            messagesMap.put(secondUserId, new LinkedList<ChatMessage>());
        }
        startNewConversatio(thisUserId, secondUserId, false);
        showChatFragment(secondUserId, secondUserToken, secondUserName);
    }

    @Override
    public void onSendNewMessage(String message, String destToken) {
        send(destToken, message);
    }

    public void getPreviousMessages(final String secondUserId, final ChatMessage incomingMessage) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(thisUserId).child("conversation_data").child(secondUserId);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    GenericTypeIndicator<ArrayList<ChatMessage>> tp = new GenericTypeIndicator<ArrayList<ChatMessage>>() {};
                    ArrayList<ChatMessage> array = dataSnapshot.getValue(tp);
                    LinkedList<ChatMessage> list;
                    if(array != null) {
                        list = new LinkedList<>(array);
                    }else{
                        list = new LinkedList<>();
                    }
                    if(incomingMessage != null)
                        list.offerLast(incomingMessage);
                    messagesMap.put(secondUserId, list);
                    mChatViewModel.selectUpdateAdapter("");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    public void saveAllMessages() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(thisUserId).child("conversation_data");
        for (Map.Entry<String, LinkedList<ChatMessage>> entry: messagesMap.entrySet()) {
            ref.child(entry.getKey()).setValue(entry.getValue());
        }
    }
}