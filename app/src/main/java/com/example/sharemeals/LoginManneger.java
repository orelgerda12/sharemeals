package com.example.sharemeals;

import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.sharemeals.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Locale;

public class LoginManneger extends AppCompatActivity implements signUpFragment.OnSignUpFragmentListener , SignInFragment.OnSignInFragmentListener , insertViewPager.OnWelcomeFragmentListener {


    String SignUpFragmentTag = "sign_up_fragment";
    String SignInFragmentTag = "sign_in_fragment";
    String WelcomeViewpagerFragmentTag = "welcome_view_pager";
    FirebaseAuth firebaseAuth =  FirebaseAuth.getInstance();
    FirebaseAuth.AuthStateListener authStateListener;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference users = database.getReference("users");
    TextInputLayout login_username , login_password;
    String Password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if(firebaseUser!=null){
            Intent sighInIntent = new Intent(LoginManneger.this, MainActivity.class);
            startActivity(sighInIntent);
            finish();
        }
        setContentView(R.layout.login_menneger);
        insertViewPager insertViewPager = new insertViewPager();
        FragmentManager welcomemanager = getSupportFragmentManager();
        FragmentTransaction WelcomeViewPagertransaction= welcomemanager.beginTransaction();
        WelcomeViewPagertransaction.add(R.id.login_fragment,insertViewPager,WelcomeViewpagerFragmentTag).addToBackStack(WelcomeViewpagerFragmentTag).commit();
        String TAG = "local_device_langh";
        Log.d(TAG,"getDisplayLanguage = " + Locale.getDefault().getDisplayLanguage());
        Log.d(TAG,"getLanguage = " + Locale.getDefault().getLanguage());
        Log.d(TAG,"Resources.getLanguage = " + Resources.getSystem().getConfiguration().locale.getLanguage());
        Log.d(TAG,"getResources.getLanguage = " + getResources().getConfiguration().locale);

        //TODO : check for hebrew font to setup
        authStateListener  = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if(firebaseUser!=null){
                    Intent sighInIntent = new Intent(LoginManneger.this, MainActivity.class);
                    startActivity(sighInIntent);
                    finish();
                }
            }};

    }
    @Override
    public void onSignUp(final User newUser) {
        firebaseAuth.createUserWithEmailAndPassword(newUser.getMail(),newUser.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            users.setValue(firebaseAuth.getUid());
                            users.child(firebaseAuth.getUid()).setValue(newUser);
                            Toast.makeText(LoginManneger.this, "isSuccessful", Toast.LENGTH_SHORT).show();
                            Intent GoToMain = new Intent(LoginManneger.this, MainActivity.class);
                            startActivity(GoToMain);
                            Fragment fragment = getSupportFragmentManager().findFragmentByTag(SignUpFragmentTag);
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();

                           }

                        }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("signup",e.toString());
            }
        });
    }

    @Override
    public void onSignIn(final String username, String Password) {
        if(!validUserName_Password(username,Password)){
            return;
        }
        firebaseAuth.signInWithEmailAndPassword(username,Password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Snackbar.make(findViewById(android.R.id.content),getString(R.string.quantity),Snackbar.LENGTH_LONG).show();
                            Intent sighInIntent = new Intent(LoginManneger.this, MainActivity.class);
                            startActivity(sighInIntent);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });


    }

    @Override
    public void onRegBtn() {
        signUpFragment signUpFragment = new signUpFragment();
        FragmentManager signupmanager = getSupportFragmentManager();
        FragmentTransaction signuptransaction= signupmanager.beginTransaction();
        signuptransaction.add(R.id.login_fragment,signUpFragment,SignUpFragmentTag).addToBackStack(SignUpFragmentTag).commit();

    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }

    public Boolean validUserName_Password(String username , String password){
        if(username.isEmpty()||password.isEmpty()){
            login_username.setError(getString(R.string.can_not_be_empty));
            return false;
        }
        if(password.isEmpty()){
            login_username.setError(getString(R.string.can_not_be_empty));
            return false;
        }
        return true;
    }

    @Override
    public void onGoToLogin() {
        SignInFragment signInFragment = new SignInFragment();
        FragmentManager signInmanager = getSupportFragmentManager();
        FragmentTransaction signIntransaction= signInmanager.beginTransaction();
        signIntransaction.add(R.id.login_fragment,signInFragment,SignInFragmentTag).addToBackStack(SignInFragmentTag).commit();
    }
}
