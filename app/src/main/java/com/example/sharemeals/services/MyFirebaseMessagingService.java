package com.example.sharemeals.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.sharemeals.MainActivity;
import com.example.sharemeals.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    final String TAG = "MyFirebaseMessaging";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            Intent intent = new Intent("message_received");
            intent.putExtra("message",remoteMessage.getData().get("message"));
            //LocalBroadcastManager.getInstance(this).
            sendBroadcast(intent);

            //if the application is not in forground post notification
            NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            String channelId = null;
            if(Build.VERSION.SDK_INT>=26) {
                channelId = "new_message";
                CharSequence channelName =  "New Message Channel"  ;
                NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
                manager.createNotificationChannel(channel);
//                builder.setChannelId("id_1");
            }
            //builder.setContentTitle("new message from topic").setContentText(remoteMessage.getData().get("message")).setSmallIcon(R.drawable.heart);


            Intent playIntent = new Intent(this, MainActivity.class);
            playIntent.putExtra("start_from_notif",true).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            PendingIntent playPendingIntent = PendingIntent.getActivity(this,0,playIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channelId);
            builder.setSmallIcon(R.drawable.heart).setContentTitle("Share Meals")
                    .setContentText("New donation...");
            builder.setContentIntent(playPendingIntent).setAutoCancel(true);

            manager.notify(2,builder.build());


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            Intent intent = new Intent("message_received");
            intent.putExtra("message",remoteMessage.getNotification().getBody());
            //LocalBroadcastManager.getInstance(this).
            sendBroadcast(intent);

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }



    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token (Service): " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token);
    }
}
