package com.example.sharemeals.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.sharemeals.DBListener;
import com.example.sharemeals.MainActivity;
import com.example.sharemeals.R;
import com.example.sharemeals.models.Meal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class DBQueryService extends Service implements DBListener.OnDBChangeListener {

    final int NOTIF_ID = 1;

    private DBListener mDBListener;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("meals");

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //mCurrentIntent = intent;
        //int counter = intent.getIntExtra("counter",10);
        mDBListener  = new DBListener(this);

        NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        String channelId = null;
        if(Build.VERSION.SDK_INT>=26) {
            channelId =  "notification_channel_DBQueryService" ;
            CharSequence channelName =  "DB Listener Channel"  ;
            int  importance = NotificationManager. IMPORTANCE_HIGH ;
            NotificationChannel notificationChannel =  new  NotificationChannel(channelId, channelName, importance);
            manager.createNotificationChannel(notificationChannel);
        }

        Intent playIntent = new Intent(this, MainActivity.class);
        playIntent.putExtra("start_from_notif",true).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent playPendingIntent = PendingIntent.getActivity(this,0,playIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channelId);
        builder.setSmallIcon(R.drawable.heart).setContentTitle("Share Meals")
                .setContentText("Waiting for donation...");
        builder.setContentIntent(playPendingIntent).setAutoCancel(true);
        startForeground(NOTIF_ID,builder.build());

//                mDatabase.child("meals_wait_for_pickup").child(key).child("kosher").setValue(true);
//                    Toast.makeText(getContext(),"send "+key,Toast.LENGTH_SHORT).show();
        //------------------------------------------------------------------------------------------------------------------------------------
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                stopForeground(true);
//            }
//        },5000);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDBChangeListener(DataSnapshot dataSnapshot) {
        Log.d("onDataChange", "onChildAdded:" + dataSnapshot.getKey() + dataSnapshot.getValue());
    }

//    private void listenToDB(){
//
//        meals.addChildEventListener(new ChildEventListener() {
////            @Override
////            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////                //mealsList.clear();
////                Log.d("onDataChange","fun called");
////                if (dataSnapshot.exists()){
////                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
////                        String meal = snapshot.getKey();
////                        //mealsList.add(meal);
////                        Log.d("onDataChange","data snapshot noy null = "+meal);
////                    }
////                    //adapter.notifyDataSetChanged();
////                }
////            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//
//
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
//                if(dataSnapshot.exists())
//                    Log.d("onDataChange", "onChildAdded:" + dataSnapshot.getKey()+dataSnapshot.getValue());
//
//                // A new comment has been added, add it to the displayed list
//
//
//                // ...
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
//                Log.d("onDataChange", "onChildChanged:" + dataSnapshot.getKey());
//
//                // A comment has changed, use the key to determine if we are displaying this
//                // comment and if so displayed the changed comment.
//
//                String commentKey = dataSnapshot.getKey();
//
//                // ...
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Log.d("onDataChange", "onChildRemoved:" + dataSnapshot.getKey());
//
//                // A comment has changed, use the key to determine if we are displaying this
//                // comment and if so remove it.
//                String commentKey = dataSnapshot.getKey();
//
//                // ...
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
//                Log.d("onDataChange", "onChildMoved:" + dataSnapshot.getKey());
//
//                // A comment has changed position, use the key to determine if we are
//                // displaying this comment and if so move it.
//
//                String commentKey = dataSnapshot.getKey();
//
//                // ...
//            }
//
//
//        });
//    }




}
