package com.example.sharemeals.models;

public class ChatMessageWrapper {
    public ChatMessage message;
    public String destinationUserId;
    public String destinationUserToken;


    public ChatMessageWrapper(ChatMessage message, String destinationUserId, String destinationUserToken) {
        this.message = message;
        this.destinationUserId = destinationUserId;
        this.destinationUserToken = destinationUserToken;
    }
}
