package com.example.sharemeals.models;

import android.content.Context;
import android.content.res.Resources;

import com.example.sharemeals.MainActivity;
import com.example.sharemeals.R;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Meal implements Serializable {

    private Context context;
    private String status;
    private String name;
    private String meal_id;
    private int quantity;
    private String category;
    private String insertion_time;
    private Boolean kosher;
    private String area;
    private String city;
    private String address;
    private String packing;
    private String last_time_for_pick; //TODO : change to datetime type
    private Boolean elrg_glutten;
    private Boolean elrg_beans;
    private Boolean elrg_lactos;
    private Boolean elrg_nuts;
    private String image_link;
    private String user_id;
    private String phone;
    private String pickup_by;

    public Meal() {
        this.kosher = false;
        this.elrg_beans = false;
        this.elrg_glutten = false;
        this.elrg_lactos = false;
        this.elrg_nuts = false;
        this.status = "הזמנה חדשה" ;
        this.pickup_by = "none";
    }

    public Meal(String name, int quantity, String category, String insertion_time, Boolean kosher, String area, String city, String address, String packing, String last_time_for_pick, Boolean elrg_glutten, Boolean elrg_beans, Boolean elrg_lactos, Boolean elrg_nuts, String image_link, String user_id, String phone) {
        this.name = name;
        this.quantity = quantity;
        this.category = category;
        this.insertion_time = insertion_time;
        this.kosher = kosher;
        this.area = area;
        this.city = city;
        this.address = address;
        this.packing = packing;
        this.last_time_for_pick = last_time_for_pick;
        this.elrg_glutten = elrg_glutten;
        this.elrg_beans = elrg_beans;
        this.elrg_lactos = elrg_lactos;
        this.elrg_nuts = elrg_nuts;
        this.image_link = image_link;
        this.user_id = user_id;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getInsertion_time() {
        return insertion_time;
    }

    public void setInsertion_time(String insertion_time) {
        this.insertion_time = insertion_time;
    }

    public Boolean getKosher() {
        return kosher;
    }

    public void setKosher(Boolean kosher) {
        this.kosher = kosher;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getLast_time_for_pick() {
        return last_time_for_pick;
    }

    public void setLast_time_for_pick(String last_time_for_pick) {
        this.last_time_for_pick = last_time_for_pick;
    }

    public Boolean getElrg_glutten() {
        return elrg_glutten;
    }

    public void setElrg_glutten(Boolean elrg_glutten) {
        this.elrg_glutten = elrg_glutten;
    }

    public Boolean getElrg_beans() {
        return elrg_beans;
    }

    public void setElrg_beans(Boolean elrg_beans) {
        this.elrg_beans = elrg_beans;
    }

    public Boolean getElrg_lactos() {
        return elrg_lactos;
    }

    public void setElrg_lactos(Boolean elrg_lactos) {
        this.elrg_lactos = elrg_lactos;
    }

    public Boolean getElrg_nuts() {
        return elrg_nuts;
    }

    public void setElrg_nuts(Boolean elrg_nuts) {
        this.elrg_nuts = elrg_nuts;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(String meal_id) {
        this.meal_id = meal_id;
    }

    public String getPickup_by() {
        return pickup_by;
    }

    public void setPickup_by(String pickup_by) {
        this.pickup_by = pickup_by;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
