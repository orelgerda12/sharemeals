package com.example.sharemeals.models;

public class Conversation {

    private String secondUserID;
    private String secondUserToken;
    private String secondUserName;
    private String thisUserId;
    private String lastMessage;
    private String lastMessageDate;
    private boolean status;


    public Conversation() {
    }


    public Conversation(String secondUserID, String secondUserToken, String secondUserName, String thisUserId, String lastMessage, String lastMessageDate, boolean status) {
        this.secondUserID = secondUserID;
        this.secondUserToken = secondUserToken;
        this.secondUserName = secondUserName;
        this.thisUserId = thisUserId;
        this.lastMessage = lastMessage;
        this.lastMessageDate = lastMessageDate;
        this.status = status;
    }


    public String getSecondUserID() {
        return secondUserID;
    }

    public void setSecondUserID(String secondUserID) {
        this.secondUserID = secondUserID;
    }

    public String getSecondUserToken() {
        return secondUserToken;
    }

    public void setSecondUserToken(String secondUserToken) {
        this.secondUserToken = secondUserToken;
    }

    public String getSecondUserName() {
        return secondUserName;
    }

    public void setSecondUserName(String secondUserName) {
        this.secondUserName = secondUserName;
    }

    public String getThisUserId() {
        return thisUserId;
    }

    public void setThisUserId(String thisUserId) {
        this.thisUserId = thisUserId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(String lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
