package com.example.sharemeals.models;

public class ChatMessage {
    String text;
    String date;
    String time;
    String senderId;
    Boolean thisUserSendThisMessage;

    public ChatMessage() {
    }

    public ChatMessage(String text, String date, String time, String senderId, Boolean thisUserSendThisMessage) {
        this.text = text;
        this.date = date;
        this.time = time;
        this.senderId = senderId;
        this.thisUserSendThisMessage = thisUserSendThisMessage;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public Boolean getThisUserSendThisMessage() {
        return thisUserSendThisMessage;
    }

    public void setThisUserSendThisMessage(Boolean thisUserSendThisMessage) {
        this.thisUserSendThisMessage = thisUserSendThisMessage;
    }
}

