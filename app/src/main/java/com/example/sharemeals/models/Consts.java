package com.example.sharemeals.models;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Consts {
    public List<String> listQuantity;
    public List<String> listCategory;
    public List<String> listCategory_heb;
    public String [] CitiesList;
    public String [] AreaList;
    public List<String> getListQuantity(){
        this.listQuantity = new ArrayList<>();
        this.listQuantity.add(0,"1");
        this.listQuantity.add(1,"2");
        this.listQuantity.add(2,"3");
        this.listQuantity.add(3,"4");
        this.listQuantity.add(4,"5");
        this.listQuantity.add(5,"5+");
        return this.listQuantity;
    }
    //TODO:add translat heb word in heb
    public List<String> getListCategory(){
        this.listCategory = new ArrayList<>();
        this.listCategory.add(0,"italiano");
        this.listCategory.add(1,"mexican");
        this.listCategory.add(2,"soups");
        this.listCategory.add(3,"american");
        this.listCategory.add(4,"israeli");
        this.listCategory.add(5,"desert");
        this.listCategory.add(6,"thai");
        this.listCategory.add(7,"vegan");
        this.listCategory.add(8,"home made");
        this.listCategory.add(9,"fast food");
        return this.listCategory;
    }

    public List<String> getListCategory_heb() {
        this.listCategory_heb = new ArrayList<>();
        this.listCategory_heb.add(0,"איטלקי");
        this.listCategory_heb.add(1,"מקסיקני");
        this.listCategory_heb.add(2,"מרקים");
        this.listCategory_heb.add(3,"אמריקאי");
        this.listCategory_heb.add(4,"ישראלי");
        this.listCategory_heb.add(5,"קינוחים");
        this.listCategory_heb.add(6,"תילאנדי");
        this.listCategory_heb.add(7,"צמחוני");
        this.listCategory_heb.add(8,"הומייד");
        this.listCategory_heb.add(9,"אוכל מהיר");
        return listCategory_heb;
    }

    public Consts()  {

        getListQuantity();
        getListCategory();
        getListCategory_heb();
        this.AreaList = new String[]{"Tel-aviv" , "Shfela" , "South" , "Eilat" , "north", "haifa" , "Gush-Dan"};

    }


    public void setListQuantity(List<String> listQuantity) {
        this.listQuantity = listQuantity;
    }

    public void setListCategory(List<String> listCategory) {
        this.listCategory = listCategory;
    }

    public String[] getCitiesList() {
        return CitiesList;
    }

    public void setCitiesList(String[] citiesList) {
        CitiesList = citiesList;
    }

    public String[] getAreaList() {
        return AreaList;
    }

    public void setAreaList(String[] areaList) {
        AreaList = areaList;
    }
}
