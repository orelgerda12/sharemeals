package com.example.sharemeals.models;

import java.util.LinkedList;

public class User {
    private String username;
    private String imageUrl;
    private String passwword;
    private String email;
    private Boolean isRestaurant;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String firstName;
    private String let;
    private String alt;
    private String city;
    private String messageToken;


    public User(String username, String imageUrl, String let, String alt, String city, String messageToken) {
        this.username = username;
        this.imageUrl = imageUrl;
        this.let = let;
        this.alt = alt;
        this.city = city;
        this.messageToken = messageToken;

    }

    public User() {
        this.isRestaurant = false;
    }


    public String getPasswword() {
        return passwword;
    }

    public void setPasswword(String passwword) {
        this.passwword = passwword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getRestaurant() {
        return isRestaurant;
    }

    public void setRestaurant(Boolean restaurant) {
        isRestaurant = restaurant;
    }

    public String getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String messageToken) {
        this.messageToken = messageToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLet() {
        return let;
    }

    public void setLet(String let) {
        this.let = let;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPassword() {
        return passwword;
    }

    public void setPassword(String passwword) {
        this.passwword = passwword;
    }

    public String getMail() {
        return email;
    }

    public void setMail(String email) {
        this.email = email;
    }
}



