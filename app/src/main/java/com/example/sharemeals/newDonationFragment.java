package com.example.sharemeals;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.os.ConfigurationCompat;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.sharemeals.models.Consts;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.example.sharemeals.models.Meal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import org.w3c.dom.Text;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import static android.app.Activity.RESULT_OK;

public class newDonationFragment extends Fragment implements AdapterView.OnItemSelectedListener,View.OnClickListener {
    private static final Pattern PHONE_PATTERN =
            Pattern.compile("^[0][5][0|2|3|4|5|9]{1}[-]{0,1}[0-9]{7}$");

    MaterialBetterSpinner spinnerQuantity, spinnerCategory;
    Context context;
    Spinner test_spinner;
    FirebaseUser user;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    View rootview;
    AutoCompleteTextView CitiesautoCompleteTextView, AreaautoCompleteTextView;
    String user_id_to_db;
    ImageButton imageButton1, delImageBtn1, pushDataBtn;
    CheckBox alrg1, alrg2, alrg3, alrg4, kosher;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_WRITE_READ = 2;
    final int TAKE_PIC_REQUEST = 1;
    final int WRITE_READ_REQUEST = 2;
    private FirebaseStorage Meal_images = FirebaseStorage.getInstance();
    StorageReference Meal_images_Ref = Meal_images.getReference();
    FirebaseDatabase pickupMeals = FirebaseDatabase.getInstance();
    DatabaseReference meals = pickupMeals.getReference("new_meals");
    String key;
    AlertDialog.Builder builder;
    String mCurrentPhotoPath , AmPm;
    TextInputLayout name , phone_number , address ;
    EditText last_pickup_time , last_pickup_date;
    Bitmap bitmap;
    ProgressBar progressBar;
    Uri photoURI;
    Consts consts;
    Meal MealObj = new Meal();
    Map<String , Boolean> alrg = new HashMap<>();
    Utils utils = new Utils();
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener dateSetListener;
    int currentHour , currentMinute;
    List<String> listCategory;
    String langhat = ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration()).get(0).getLanguage();



    public static newDonationFragment newInstance() {
        newDonationFragment fragment = new newDonationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.new_donation_design, container, false);
        spinnerQuantity = rootview.findViewById(R.id.quantity_spinner);
        spinnerCategory = rootview.findViewById(R.id.category_spinner);
        CitiesautoCompleteTextView = rootview.findViewById(R.id.city_autocomplete);
        AreaautoCompleteTextView = rootview.findViewById(R.id.area_autocomplete);
        consts = new Consts();
        List<String> listQuantity = consts.getListQuantity();
        if(langhat != "iw"){
            listCategory = consts.getListCategory();
        }else{
            listCategory = consts.getListCategory_heb();
        }

        String[] CityList = getResources().getStringArray(R.array.cities_heb);
        String[] AreaList = getResources().getStringArray(R.array.area_listheb);
        // ArrayAdapterSpinner
        ArrayAdapter<String> arrayAdapterSpinner_quantity;
        arrayAdapterSpinner_quantity = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, listQuantity);
        spinnerQuantity.setAdapter(arrayAdapterSpinner_quantity);
        spinnerQuantity.setOnItemSelectedListener(this);
        ArrayAdapter<String> CitiesautoCompleteaAdapter;
        CitiesautoCompleteaAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, CityList);
        CitiesautoCompleteTextView.setAdapter(CitiesautoCompleteaAdapter);
        ArrayAdapter<String> AreaautoCompleteaAdapter;
        AreaautoCompleteaAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, AreaList);
        AreaautoCompleteTextView.setAdapter(AreaautoCompleteaAdapter);
        ArrayAdapter<String> arrayAdapterSpinner_category;
        arrayAdapterSpinner_category = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, listCategory);
        spinnerCategory.setAdapter(arrayAdapterSpinner_category);
        user = firebaseAuth.getCurrentUser();
        user_id_to_db = user.getUid();
        imageButton1 = rootview.findViewById(R.id.donation_add_pic_btn);
        delImageBtn1 = rootview.findViewById(R.id.delete_btn_new_donation);
        progressBar = rootview.findViewById(R.id.progress_circular);
        pushDataBtn = rootview.findViewById(R.id.add_donation_btn);
        name = rootview.findViewById(R.id.new_meal_input_name);
        address = rootview.findViewById(R.id.new_meal_address_input);
        phone_number = rootview.findViewById(R.id.new_meal_phone_number);
        alrg1 = rootview.findViewById(R.id.checkbox_alrg1);
        alrg2 = rootview.findViewById(R.id.checkbox_alrg2);
        alrg3 = rootview.findViewById(R.id.checkbox_alrg3);
        alrg4 = rootview.findViewById(R.id.checkbox_alrg4);
        kosher = rootview.findViewById(R.id.new_meal_is_kosher);
        last_pickup_time = rootview.findViewById(R.id.new_meal_last_pickup_time);
        last_pickup_date = rootview.findViewById(R.id.new_meal_last_pickup_date);
        last_pickup_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault,
                        dateSetListener,year,month,day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                datePickerDialog.show();
                dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month +1;
                        String date = dayOfMonth + "/" + month + "/" + year;
                        last_pickup_date.setText(date);
                    }
                };



            }
        });
        last_pickup_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if(hourOfDay > 12){
                            AmPm = "pm";
                        }else{
                            AmPm="am";
                        }
                        last_pickup_time.setText(String.format("02%d:02%d",hourOfDay,minute) + AmPm);
                    }
                },currentHour ,currentMinute,false );
                timePickerDialog.show();
            }
        });
        alrg1.setOnClickListener(this);
        alrg2.setOnClickListener(this);
        alrg3.setOnClickListener(this);
        alrg4.setOnClickListener(this);
        kosher.setOnClickListener(this);
        imageButton1.setOnClickListener(new View.OnClickListener() {
            //            Todo: insert the dialog
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    int hasCameraPermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
                    int hasWritePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    int hasReadPermission = getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                    if (hasCameraPermission == PackageManager.PERMISSION_GRANTED &&
                            hasWritePermission == PackageManager.PERMISSION_GRANTED && hasReadPermission == PackageManager.PERMISSION_GRANTED) {
                        TakePic();
                    } else { // Permission_Denied
                        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PIC_REQUEST);
                        }
                        if (hasWritePermission != PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                Toast.makeText(getContext(), "we need permissions please!", Toast.LENGTH_SHORT).show();

                            }
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_READ_REQUEST);
                        }
                        if (hasReadPermission != PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(),
                                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                Toast.makeText(getContext(), "we need permissions now!", Toast.LENGTH_SHORT).show();

                            }
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_READ_REQUEST);
                        }
                    }


                } else {
                    TakePic();
                }

            }
        });
        delImageBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageButton1.setImageResource(R.drawable.ic_to_upload_image);
                delImageBtn1.setVisibility(View.INVISIBLE);
            }
        });
        //put data
        pushDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateQuantity()|!validatePhone()|!validateAdress()||!validImage()) {
                    return;
                }
                key = meals.push().getKey();
                MealObj.setInsertion_time(utils.GeturrentDateTime());
                MealObj.setQuantity(Integer.parseInt(spinnerQuantity.getText().toString()));
                MealObj.setCategory(spinnerCategory.getText().toString());
                MealObj.setCity(CitiesautoCompleteTextView.getText().toString());
                MealObj.setArea(AreaautoCompleteTextView.getText().toString());
                MealObj.setAddress(address.getEditText().getText().toString());
                MealObj.setPhone(phone_number.getEditText().getText().toString());
                MealObj.setName(name.getEditText().getText().toString());
                MealObj.setUser_id(user.getUid());
                MealObj.setImage_link("JPEG_" + key);
                MealObj.setLast_time_for_pick(last_pickup_time.getText().toString() + " - " + last_pickup_date.getText().toString());
                MealObj.setMeal_id(key);

                meals.child(key).setValue(MealObj).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.push_succses),Snackbar.LENGTH_LONG).show();
                        //todo: close fragment
                    }
                });
                if (bitmap!=null){
                meals.child(key).child("image_link").setValue(MealObj.getImage_link());}
                if(MealObj.getLast_time_for_pick()!=null){
                meals.child(key).child("last_time_for_pick").setValue(MealObj.getLast_time_for_pick());}
                UploadToFirebasetore();


                //Push to database

            }
        });
        return (rootview);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAKE_PIC_REQUEST || requestCode == WRITE_READ_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                TakePic();
            } else {
                Snackbar.make(getActivity().findViewById(android.R.id.content), "sorry cant work without permission", Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "sorry cant work without permission", Snackbar.LENGTH_LONG).show();
        }

    }


    private void TakePic() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                throw new ClassCastException("error occured while creating the File");
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    private void ImageFromGallery() {
        CropImage.startPickImageActivity(getActivity());
    }

    private void croprequest(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_IMAGE_CAPTURE || requestCode == REQUEST_WRITE_READ) && resultCode == getActivity().RESULT_OK) {
            galleryAddPic();
            delImageBtn1.setVisibility(View.VISIBLE);
            setPic(imageButton1);

        }

    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "JPEG_" + key;
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private File getExternalFilesDir(String directoryPictures) {
        throw new ClassCastException("error occured while creating the File");
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public void setPic(ImageButton imageButton) {
        // Get the dimensions of the View
        int targetW = imageButton1.getWidth();
        int targetH = imageButton1.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        imageButton.setImageBitmap(bitmap);
    }

    public void UploadToFirebasetore() {
        StorageReference ref = Meal_images_Ref.child("meals_images/" + "JPEG_" + key);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 99, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = ref.putBytes(data);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "Fail" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                Toast.makeText(getActivity(), "Uplaoded " + (int) progress + " %", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        MealObj.setQuantity(Integer.parseInt(parent.getItemAtPosition(position).toString()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.checkbox_alrg1:
                    MealObj.setElrg_glutten(alrg1.isChecked());
                break;
            case R.id.checkbox_alrg2:
                    MealObj.setElrg_nuts(alrg2.isChecked());
                break;
            case R.id.checkbox_alrg3:
                    MealObj.setElrg_beans(alrg3.isChecked());
                break;
            case R.id.checkbox_alrg4:
                    MealObj.setElrg_lactos(alrg4.isChecked());
                break;
            case R.id.new_meal_is_kosher:
                MealObj.setElrg_lactos(kosher.isChecked());
                break;
        }
    }
    private boolean validateQuantity() {
        String QuantityInput = spinnerQuantity.getText().toString().trim();

        if (QuantityInput.isEmpty()) {
            Snackbar.make(getActivity().findViewById(android.R.id.content),getString(R.string.quantity),Snackbar.LENGTH_LONG).show();
            try {
                //set time in mili
                Thread.sleep(500);

            }catch (Exception e){
                e.printStackTrace();
            }
            return false;
        } else {
            return true;
        }
    }
    private boolean validatePhone() {
        String phoneInput = phone_number.getEditText().getText().toString().trim();
        if (phoneInput.isEmpty()) {
            phone_number.setError(getString(R.string.can_not_be_empty));
            try {
                //set time in mili
                Thread.sleep(500);

            }catch (Exception e){
                e.printStackTrace();
            }Snackbar.make(getActivity().findViewById(android.R.id.content),getString(R.string.can_not_be_empty),Snackbar.LENGTH_LONG).show();
            try {
                //set time in mili
                Thread.sleep(500);

            }catch (Exception e){
                e.printStackTrace();}
            return false;
        } else if (!PHONE_PATTERN.matcher(phoneInput).matches()) {
            phone_number.setError(getString(R.string.phone_number));

            Snackbar.make(getActivity().findViewById(android.R.id.content),getString(R.string.phone_number),Snackbar.LENGTH_LONG).show();
            try {
                //set time in mili
                Thread.sleep(500);

            }catch (Exception e){
                e.printStackTrace();}
            return false;
        } else {
            phone_number.setError(null);
            return true;
        }
    }
    private boolean validateAdress() {
        String city = CitiesautoCompleteTextView.getText().toString();
        String area = AreaautoCompleteTextView.getText().toString();
        String address_et = address.getEditText().getText().toString();
        if (city.isEmpty()|area.isEmpty()|address_et.isEmpty()) {
            phone_number.setError(getString(R.string.can_not_be_empty));
            Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.can_not_be_empty),Snackbar.LENGTH_LONG).show();
            return false;
        } else {

            return true;
        }
    }
    public boolean validImage(){
        if(bitmap == null){
            Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.image_error),Snackbar.LENGTH_LONG).show();
            return false ;
        }
        else{
            return true;
        }
    }


}

